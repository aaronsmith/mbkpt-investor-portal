const defaultTheme = require('@tailwindcss/postcss7-compat/defaultTheme')
const colors = require('@tailwindcss/postcss7-compat/colors')

module.exports = {
  purge: [
    './resources/**/*.blade.php',
    './resources/**/*.js',
    './resources/**/*.vue',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    colors: {
      orange: colors.orange,
      gray: colors.gray,
      indigo: colors.indigo,
      red: colors.red,
      green: colors.green,
      blue: colors.lightBlue,
      teal: colors.emerald,
      white: colors.white,
      black: colors.black,
      lime: colors.lime,
    },
    extend: {
      fontFamily: {
        sans: ['Inter var', ...defaultTheme.fontFamily.sans],
      },
    },
  },
  variants: {
    extend: {
      opacity: ['disabled'],
      cursor: ['disabled']
    }
  },
  plugins: [
      require('@tailwindcss/forms'),
  ],
}
