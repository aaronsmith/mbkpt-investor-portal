@extends('_layouts.main')

@section('content')
    <!-- This example requires Tailwind CSS v2.0+ -->
    <div class="fixed inset-0 overflow-hidden hidden" id="terms">
        <div class="absolute inset-0 overflow-hidden">
            <section class="absolute inset-y-0 right-0 pl-10 max-w-full flex" aria-labelledby="slide-over-heading">
                <!--
                  Slide-over panel, show/hide based on slide-over state.

                  Entering: "transform transition ease-in-out duration-500 sm:duration-700"
                    From: "translate-x-full"
                    To: "translate-x-0"
                  Leaving: "transform transition ease-in-out duration-500 sm:duration-700"
                    From: "translate-x-0"
                    To: "translate-x-full"
                -->
                <div class="w-screen max-w-md">
                    <div class="h-full flex flex-col py-6 bg-white shadow-lg overflow-y-scroll">
                        <div class="px-4 sm:px-6">
                            <div class="flex items-start justify-between">
                                <h2 id="slide-over-heading" class="text-lg font-medium text-gray-900">
                                    Privacy Policy
                                </h2>
                                <div class="ml-3 h-7 flex items-center">
                                    <button onclick="hideTerms()" class="bg-white rounded-md text-gray-400 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                                        <span class="sr-only">Close panel</span>
                                        <!-- Heroicon name: x -->
                                        <svg class="h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                                        </svg>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="mt-6 relative flex-1 px-4 sm:px-6">
                            <!-- Replace with your content -->
                            <div class="absolute inset-0 px-4 sm:px-6 text-sm text-gray-500">
                                <p class="mb-4">
                                    This Privacy Policy describes how your personal information is collected, used, and shared when you visit https://investors.mbkpt.com (the “Site”).
                                </p>

                                <p class="mb-1 font-medium">
                                    Personal information we collect
                                </p>

                                <p class="mb-4">
                                    When you visit the Site, we may automatically collect certain information about your device, including information about your web browser, IP address, time zone, and some of the cookies that are installed on your device. Additionally, as you browse the Site, we collect information about the documents that you view, what websites or search terms referred you to the Site, and information about how you interact with the Site. We refer to this automatically-collected information as “Device Information”.
                                </p>

                                <p class="mb-4">
                                    We may collect Device Information using the following technologies: -“Cookies” are data files that are placed on your device or computer and often include an anonymous unique identifier. For more information about cookies, and how to disable cookies, visit http://www.allaboutcookies.org. -“Log files” track actions occurring on the Site, and collect data including your IP address, browser type, Internet service provider, referring/exit pages, and date/time stamps. -“Web beacons”, “tags”, and “pixels” are electronic files used to record information about how you browse the Site.
                                </p>

                                <p class="mb-4">
                                    When we talk about “Personal Information” in this Privacy Policy, we are talking about Device Information.
                                </p>

                                <p class="mb-1 font-medium">
                                    How do we use your personal information?
                                </p>

                                <p class="mb-4">
                                    We use the Information that we collect generally tocommunicate with you.
                                </p>

                                <p class="mb-4">
                                    We use the Device Information that we collect to help us screen for potential risk and fraud (in particular, your IP address), and more generally to improve and optimize our Site (for example, by generating analytics about how our investors browse and interact with the Site).
                                </p>

                                <p class="mb-1 font-medium">
                                    Sharing you personal Information
                                </p>

                                <p class="mb-4">
                                    We do not share your Personal Information with third parties.
                                </p>

                                <p class="mb-4">
                                    We may also share your Personal Information to comply with applicable laws and regulations, to respond to a subpoena, search warrant or other lawful request for information we receive, or to otherwise protect our rights.
                                </p>

                                <p class="mb-1 font-medium">
                                    Your rights
                                </p>

                                <p class="mb-4">
                                    If you are a European resident, you have the right to access personal information we hold about you and to ask that your personal information be corrected, updated, or deleted. If you would like to exercise this right, please contact us through the contactinformation below.
                                </p>

                                <p class="mb-4">
                                Additionally, if you are a European resident we note that we are processing your information to pursue our legitimate business interests listed above. Additionally, please note that your information will be transferred outside of Europe, including to the United States.
                                </p>

                                <p class="mb-1 font-medium">
                                    Data retention
                                </p>

                                <p class="mb-4">
                                    When you interact with the Site, we will maintaina recordof your interaction unless and until you ask us to delete this information.
                                </p>

                                <p class="mb-1 font-medium">Changes</p>
                                <p class="mb-4">
                                    We may update this privacy policy from time to time in order to reflect, for example, changes to our practices or for other operational, legal or regulatory reasons.
                                </p>

                                <p class="mb-1 font-medium">Contact us</p>
                                <p class="mb-4">
                                For more information about our privacy practices, if you have questions, or if you would like to make a complaint, please contact us by e-mail at info@mbckeeptrustcom or by mail using the details provided below:
                                </p>

                                <p class="mb-4">
                                    Mutual Benefits Keep Policy Trust<br>
                                    [Re: Privacy Compliance Officer]
                                </p>

                                <p class="mb-4">
                                    c/o KapilaMukamal, LLP  1000 South Federal Highway, Suite 200, Fort Lauderdale, FL 33316
                                </p>

                            </div>
                            <!-- /End replace -->
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>


    <div class="sm:mx-auto sm:w-full sm:max-w-md mb-10">
        <h2 class="text-center text-3xl font-light text-gray-500">
            Sign Up
        </h2>
        <p class="mt-2 text-center text text-gray-600 max-w">
            Or
            <a href="{{ route('login') }}" class="font-medium text-lime-600 hover:text-lime-500">
                Sign in to your account
            </a>
        </p>
    </div>


    <div class="mt-8 sm:mx-auto sm:w-full sm:max-w-2xl">

        @if ($errors->any())
            <div class="rounded-md bg-red-100 p-4 w-full mx-auto mb-6">
                <div class="flex">
                    <div class="flex-shrink-0">
                        <!-- Heroicon name: x-circle -->
                        <svg class="h-5 w-5 text-red-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                            <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zM8.707 7.293a1 1 0 00-1.414 1.414L8.586 10l-1.293 1.293a1 1 0 101.414 1.414L10 11.414l1.293 1.293a1 1 0 001.414-1.414L11.414 10l1.293-1.293a1 1 0 00-1.414-1.414L10 8.586 8.707 7.293z" clip-rule="evenodd" />
                        </svg>
                    </div>
                    <div class="ml-3">
                        <h3 class="font-medium text-red-800">
                            Your submission has some errors.
                        </h3>
                        <div class="mt-2 text-sm text-red-700">
                            <ul class="list-disc pl-5 space-y-1">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        @else
            <div class="rounded-md bg-lime-100 p-4 mb-6">
                <div class="flex">
                    <div class="flex-shrink-0">
                        <!-- Heroicon name: check-circle -->
                        <svg class="h-5 w-5 text-lime-600" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                            <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clip-rule="evenodd" />
                        </svg>
                    </div>
                    <div class="ml-3">
                        <h3 class="font-medium text-lime-800">
                            Welcome {{ Auth::user()->firstName }}.
                        </h3>
                        <div class="mt-1 text-sm text-lime-700">
                            <p>
                                Your identity was successfully validated.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        @endif



        <div class="flex bg-gray-50 py-8 px-4 shadow sm:rounded-lg sm:px-10">

            <!-- This example requires Tailwind CSS v2.0+ -->
            <nav aria-label="Progress" class="mr-10 flex-1">
                <ol class="overflow-hidden">
                    <li class="relative pb-10">
                        <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-lime-600" aria-hidden="true"></div>
                        <a href="#" class="relative flex items-start group">
                                <span class="h-9 flex items-center">
                                  <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-lime-600 rounded-full group-hover:bg-lime-800">
                                    <svg class="w-5 h-5 text-white" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                      <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                                    </svg>
                                  </span>
                                </span>
                            <span class="ml-4 min-w-0 flex flex-col">
                                  <span class="text-xs font-semibold uppercase tracking-wide">Validate your identity</span>
                                  <span class="text-sm text-gray-500">We need to verify your information.</span>
                                </span>
                        </a>
                    </li>

                    <li class="relative pb-10">
                        <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-lime-600" aria-hidden="true"></div>
                        <a href="#" class="relative flex items-start group">
                                <span class="h-9 flex items-center">
                                  <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-lime-600 rounded-full group-hover:bg-lime-800">
                                    <svg class="w-5 h-5 text-white" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                      <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                                    </svg>
                                  </span>
                                </span>
                            <span class="ml-4 min-w-0 flex flex-col">
                                  <span class="text-xs font-semibold uppercase tracking-wide">Check your inbox</span>
                                  <span class="text-sm text-gray-500">Be sure you have entered a valid email.</span>
                                </span>
                        </a>
                    </li>

                    <li class="relative pb-10">
                        <a href="#" class="relative flex items-start group" aria-current="step">
                                <span class="h-9 flex items-center" aria-hidden="true">
                                  <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-lime-600 rounded-full">
                                    <span class="h-2.5 w-2.5 bg-lime-600 rounded-full"></span>
                                  </span>
                                </span>
                            <span class="ml-4 min-w-0 flex flex-col">
                                  <span class="text-xs font-semibold uppercase tracking-wide text-lime-600">Create a password</span>
                                  <span class="text-sm text-gray-500">Protect your account with a password.</span>
                                </span>
                        </a>
                    </li>
                </ol>
            </nav>


            <form class="flex-1" action="{{ route('signup.password.save') }}" method="POST" autocomplete="off">
                @csrf
                <div class="mb-6">
                    <label for="email" class="block text-sm font-medium text-gray-700">
                        Password
                    </label>
                    <div class="mt-1">
                        <input id="email" autocomplete="off" name="password" type="password" autocomplete="email" class="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-lime-500 focus:border-lime-500 sm:text-sm">
                    </div>
                </div>

                <div class="mb-6">
                    <label for="password" class="block text-sm font-medium text-gray-700">
                        Re-type password
                    </label>
                    <div class="mt-1">
                        <input id="email" autocomplete="off" name="password_confirmation" type="password" autocomplete="email" class="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-lime-500 focus:border-lime-500 sm:text-sm">
                    </div>
                </div>

                <div class="flex items-center justify-between mb-4">
                    <div class="flex items-center">
                        <input id="remember_me" name="terms_and_conditions" type="checkbox" class="h-4 w-4 text-lime-600 focus:ring-lime-500 border-gray-300 rounded">
                        <label class="ml-2 block text-sm text-gray-900">
                            By clicking continue you agree to our <span class="underline cursor-pointer" onclick="viewTerms()">Privacy Policy.</span>
                        </label>
                    </div>
                </div>

                <div>
                    <button type="submit" class="w-full flex justify-center py-2 px-4 border border-transparent shadow-lg text-sm font-semibold text-white bg-lime-500 hover:bg-lime-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-lime-500">
                        Continue
                    </button>
                </div>
            </form>
        </div>
    </div>

    <script>
        function viewTerms(){
            document.getElementById("terms").classList.remove('hidden')
        }

        function hideTerms(){
            document.getElementById("terms").classList.add('hidden')
        }
    </script>
@endsection