@component('mail::message')
## Account Approved

Good news! Your information was reviewed and your account was approved. You need to setup a password now.

@component('mail::button', ['url' => env('APP_URL') . '/signup/' . $user->id . '/password'])
Finish Account Setup
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent