@component('mail::message')
## Question Answered

Good news! Your question was reviewed and answered by one of our team members.

@component('mail::button', ['url' => env('APP_URL') . '/questions/' . $question->id])
View Answer
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
