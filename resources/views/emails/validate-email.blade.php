@component('mail::message')
To complete your registration, click the button below:

@component('mail::button', ['url' => env('APP_URL') . '/validate-email/' . $user->token ])
Finalize Account Setup
@endcomponent

Thanks,<br>
MBC Keep Trust
@endcomponent