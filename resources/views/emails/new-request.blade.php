@component('mail::message')
## New Request

There are new account validation requests, click the following button to view them.

@component('mail::button', ['url' => env('APP_URL') . '/admin/requests' ])
View Requests
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent