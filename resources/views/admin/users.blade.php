@extends('_layouts.admin')

@section('content')
    @if(session()->has('success'))
        <div id="notification-popup" aria-live="assertive" class="transition-all opacity-100 duration-500 fixed inset-0 flex items-end px-4 py-6 pointer-events-none sm:p-4 sm:items-start">
            <div class="w-full flex flex-col items-center space-y-4 sm:items-end">
                <!--
                  Notification panel, dynamically insert this into the live region when it needs to be displayed

                  Entering: "transform ease-out duration-300 transition"
                    From: "translate-y-2 opacity-0 sm:translate-y-0 sm:translate-x-2"
                    To: "translate-y-0 opacity-100 sm:translate-x-0"
                  Leaving: "transition ease-in duration-100"
                    From: "opacity-100"
                    To: "opacity-0"
                -->
                <div class="max-w-sm w-full bg-white shadow-lg rounded-lg pointer-events-auto ring-1 ring-black ring-opacity-5 overflow-hidden">
                    <div class="p-4">
                        <div class="flex items-start">
                            <div class="flex-shrink-0">
                                <!-- Heroicon name: outline/check-circle -->
                                <svg class="h-6 w-6 text-green-400" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z" />
                                </svg>
                            </div>
                            <div class="ml-3 w-0 flex-1 pt-0.5">
                                <p class="text-sm font-medium text-gray-900">
                                    Successfully updated!
                                </p>
                                <p class="mt-1 text-sm text-gray-500">
                                    The user {{ session()->get('success') }} was updated.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            setTimeout(() => {
                document.getElementById('notification-popup').classList.remove('opacity-100')
                document.getElementById('notification-popup').classList.add('opacity-0')
            }, 3000)
        </script>

    @endif

    <div class="flex justify-between items-center">
        <form action="?" method="get" class="rounded-md mb-6 flex items-end">
            <div class="mr-6">
                <label for="name"
                       class="block mb-2 text-sm font-medium leading-5 text-gray-700 sm:mt-px sm:pt-2">
                    Registration Status
                </label>
                <div class="max-w-2xl mt-1 sm:mt-0 sm:col-span-2">
                    <select name="is_registered" class="py-2 px-3 border border-gray-300 block rounded form-select w-full transition duration-150 ease-in-out sm:text-sm sm:leading-5">
                        <option value="" {{ (empty($is_registered)) ? 'selected' : '' }}>Any</option>
                        <option value="yes" {{ ($is_registered=='yes') ? 'selected' : '' }}>Registered</option>
                        <option value="no" {{ ($is_registered=='no') ? 'selected' : '' }}>Non-initiated</option>
                        <option value="failed" {{ ($is_registered=='failed') ? 'selected' : '' }}>Failed</option>
                    </select>

                </div>
            </div>

            <div class="mr-6">
                <label for="name"
                       class="block mb-2 text-sm font-medium leading-5 text-gray-700 sm:mt-px sm:pt-2">
                    Has Downloaded?
                </label>
                <div class="max-w-2xl mt-1 sm:mt-0 sm:col-span-2">
                    <select name="has_downloaded" class="py-2 px-3 border border-gray-300 block rounded form-select w-full transition duration-150 ease-in-out sm:text-sm sm:leading-5">
                        <option value="" {{ (empty($has_downloaded)) ? 'selected' : '' }}>Any</option>
                        <option value="yes" {{ ($has_downloaded=='yes') ? 'selected' : '' }}>Yes</option>
                        <option value="no" {{ ($has_downloaded=='no') ? 'selected' : '' }}>No</option>
                    </select>

                </div>
            </div>

            <div class="mr-6">
                <label for="name" class="block mb-2 text-sm font-medium leading-5 text-gray-700 sm:mt-px sm:pt-2">
                    Search
                </label>
                <div class="max-w-2xl mt-1 sm:mt-0 sm:col-span-2">
                    <input value="{{ $search_st }}" type="text" name="search_st" id="search_st" class="py-2 px-3 border border-gray-300 rounded block w-full transition duration-150 ease-in-out sm:text-sm sm:leading-5" />
                </div>
            </div>

            <div class="flex items-center">
                <button type="submit" class="mr-3 inline-flex items-center px-3 py-2 border border-transparent text-sm leading-4 font-medium rounded-md shadow-sm text-white bg-indigo-600 sm:leading-5 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                    Filter
                </button>

                <div class="text-indigo-600 underline text-sm">
                    <a href="/admin/users">Clear filters</a>
                </div>
            </div>

        </form>

        <div class="text-right">Total registered users
            <span class="inline-flex items-center px-3 py-0.5 rounded-full text-sm font-medium bg-green-200 text-green-800">
              {{ $totalRegistered }}
            </span>
        </div>
    </div>


    @if($users->isEmpty())
        <div class="flex items-center text-orange-600">
            <svg class="h-6 w-6 mr-2" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 9v2m0 4h.01m-6.938 4h13.856c1.54 0 2.502-1.667 1.732-3L13.732 4c-.77-1.333-2.694-1.333-3.464 0L3.34 16c-.77 1.333.192 3 1.732 3z" />
            </svg>

            <div>
                No users found.
            </div>
        </div>
    @else

        <div class="flex flex-col mt-2">
            <div class="align-middle min-w-full overflow-x-auto shadow overflow-hidden sm:rounded-lg">
                <table class="min-w-full divide-y divide-gray-200">
                    <thead>
                    <tr>
                        <th class="whitespace-nowrap px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                User
                        </th>
                        <th class="whitespace-nowrap px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                            Is Registered?
                        </th>
                        <th class="whitespace-nowrap px-6 py-3 bg-gray-50 text-xs font-medium text-gray-500 uppercase tracking-wider">
                            Investor number
                        </th>
                        <th class="whitespace-nowrap px-6 py-3 bg-gray-50 text-xs font-medium text-gray-500 uppercase tracking-wider">
                            AP number
                        </th>
                        <th class="whitespace-nowrap px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                            Has downloaded?
                        </th>
                        <th class="whitespace-nowrap px-6 py-3 bg-gray-50 text-xs font-medium text-gray-500 uppercase tracking-wider">
                            &nbsp;
                        </th>
                    </tr>
                    </thead>
                    <tbody class="bg-white divide-y divide-gray-200">

                    @foreach($users as $user)
                        <tr class="bg-white">
                            <td class="max-w-0 w-full px-6 py-4 whitespace-nowrap text-sm text-gray-900">
                                <div>
                                    {{ $user->name }}
                                </div>
                                <div class="text-xs text-gray-500">
                                    {{ $user->email }}
                                </div>
                            </td>
                            <td class="max-w-0 w-full px-6 py-4 whitespace-nowrap text-sm text-gray-900">
                                @if($user->password)
                                    <svg class="h-5 w-5 text-lime-600" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 13l4 4L19 7" />
                                    </svg>
                                @endif
                            </td>
                            <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                {{ $user->investor_number }}
                            </td>
                            <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                {{ $user->apnumber }}
                            </td>
                            <td class="px-6 py-4 text-right whitespace-nowrap text-sm text-gray-500">
                                @if($user->logs()->where('log_item_id', 12)->exists())
                                    <svg class="h-5 w-5 text-lime-600" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 13l4 4L19 7" />
                                    </svg>
                                @else
                                    &nbsp;
                                @endif
                            </td>
                            <td class="px-6 py-4 text-right whitespace-nowrap text-sm">
                                @if(!$user->logs->isEmpty())
                                    <a href="{{ route('admin.users.history', $user->id) }}" class="text-indigo-600 font-medium underline">History</a>
                                @endif

                                &nbsp;&nbsp;

                                <a href="{{ route('admin.users.edit', $user->id) }}" class="text-indigo-600 font-medium underline">View / Edit</a>

                            </td>
                        </tr>
                    @endforeach

                </tbody>
            </table>
        </div>

            <div class="mt-3">
                {{ $users->links() }}
            </div>
    </div>

@endif
@endsection
