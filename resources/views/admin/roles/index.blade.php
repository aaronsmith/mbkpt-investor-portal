@extends('_layouts.admin')

@section('content')
    @if(session()->has('success'))
        <div id="notification-popup" aria-live="assertive" class="transition-all opacity-100 duration-500 fixed inset-0 flex items-end px-4 py-6 pointer-events-none sm:p-4 sm:items-start">
        <div class="w-full flex flex-col items-center space-y-4 sm:items-end">
            <!--
              Notification panel, dynamically insert this into the live region when it needs to be displayed

              Entering: "transform ease-out duration-300 transition"
                From: "translate-y-2 opacity-0 sm:translate-y-0 sm:translate-x-2"
                To: "translate-y-0 opacity-100 sm:translate-x-0"
              Leaving: "transition ease-in duration-100"
                From: "opacity-100"
                To: "opacity-0"
            -->
            <div class="max-w-sm w-full bg-white shadow-lg rounded-lg pointer-events-auto ring-1 ring-black ring-opacity-5 overflow-hidden">
                <div class="p-4">
                    <div class="flex items-start">
                        <div class="flex-shrink-0">
                            <!-- Heroicon name: outline/check-circle -->
                            <svg class="h-6 w-6 text-green-400" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z" />
                            </svg>
                        </div>
                        <div class="ml-3 w-0 flex-1 pt-0.5">
                            <p class="text-sm font-medium text-gray-900">
                                Successfully updated!
                            </p>
                            <p class="mt-1 text-sm text-gray-500">
                                Permissions for this role were updated.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <script>
            setTimeout(() => {
                document.getElementById('notification-popup').classList.remove('opacity-100')
                document.getElementById('notification-popup').classList.add('opacity-0')
            }, 3000)
        </script>

    @endif


    @if($roles->isEmpty())
        <div class="flex items-center text-orange-600">
            <svg class="h-6 w-6 mr-2" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 9v2m0 4h.01m-6.938 4h13.856c1.54 0 2.502-1.667 1.732-3L13.732 4c-.77-1.333-2.694-1.333-3.464 0L3.34 16c-.77 1.333.192 3 1.732 3z" />
            </svg>

            <div>
                No roles found.
            </div>
        </div>
    @else

        <div class="flex">
            <div class="mr-10">
                <div class="mb-4 text-gray-600">Select a role to edit</div>
                <nav class="space-y-2" aria-label="Sidebar">
                    <!-- Current: "bg-gray-100 text-gray-900", Default: "text-gray-600 hover:bg-gray-50 hover:text-gray-900" -->

                    @foreach($roles as $role)
                        <a href="?current_role={{ $role->id }}" class="bg-gray-200 @if(request()->has('current_role') & !empty(request()->get('current_role')) && request()->get('current_role') == $role->id) bg-gray-700 text-white @endif text-gray-900 flex items-center px-3 py-2 text-sm font-medium rounded-md" aria-current="page">
                            <span class="truncate">
                                {{ $role->name }}
                            </span>

                            @if(request()->has('current_role') & !empty(request()->get('current_role')) && request()->get('current_role') == $role->id)
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M13 7l5 5m0 0l-5 5m5-5H6" />
                            </svg>
                            @endif
                        </a>
                    @endforeach

                </nav>
            </div>

            @if(request()->has('current_role') && !empty(request()->get('current_role')))

            <form action="{{ route('admin.roles.update', request()->get('current_role')) }}" method="post" class="mr-10">
                @csrf

                <div class="mb-4 text-gray-600">Permissions for this role</div>

                @foreach($permissions as $permission)
                <label class="relative mb-2 flex cursor-pointer">
                    <input type="checkbox" @if($currentRole->hasPermission($permission->name)) checked @endif name="permissions[]" value="{{ $permission->id }}" class="h-4 w-4 mt-0.5 cursor-pointer text-indigo-600 border-gray-300 focus:ring-indigo-500" aria-labelledby="privacy-setting-1-label" aria-describedby="privacy-setting-1-description">
                    <div class="ml-3 flex flex-col">
                        <!-- Checked: "text-indigo-900", Not Checked: "text-gray-900" -->
                        <span id="privacy-setting-1-label" class="text-gray-900 block text-sm font-medium">
                          {{ $permission->name }}
                        </span>
                    </div>
                </label>
                @endforeach

                <div class="mt-4">
                    <button id="button" type="submit" class="flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-indigo-500 hover:bg-indigo-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                        Update
                    </button>
                </div>

            </form>
            @endif
        </div>


    @endif

    <form action="{{ route('admin.roles.save') }}" method="post" class="mt-12 max-w-xs">
        @csrf

        <div class="mb-3">
            <label class="block text-sm font-medium text-gray-700">
                Add a new role
            </label>
            <div class="mt-1">
                <input placeholder="Role name" autocomplete="off" name="name" type="text" class="apnumber appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-lime-500 focus:border-lime-500 sm:text-sm">
            </div>
        </div>

        <div>
            <button id="button" type="submit" class="flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-indigo-500 hover:bg-indigo-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                Save
            </button>
        </div>
    </form>

@endsection
