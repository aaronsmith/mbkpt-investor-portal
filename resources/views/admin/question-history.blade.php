@extends('_layouts.admin')

@section('content')

    <form action="?" method="get" class="rounded-md mb-6 flex items-end">
        <div class="mr-6">
            <label for="name"
                   class="block mb-2 text-sm font-medium leading-5 text-gray-700 sm:mt-px sm:pt-2">
                Is Answered?
            </label>
            <div class="max-w-2xl mt-1 sm:mt-0 sm:col-span-2">
                <select name="is_answered" class="py-2 px-3 border border-gray-300 block rounded form-select w-full transition duration-150 ease-in-out sm:text-sm sm:leading-5">
                    <option value="" {{ (empty($is_answered)) ? 'selected' : '' }}>Any</option>
                    <option value="yes" {{ ($is_answered=='yes') ? 'selected' : '' }}>Yes</option>
                    <option value="no" {{ ($is_answered=='no') ? 'selected' : '' }}>No</option>
                </select>

            </div>
        </div>

        <div class="mr-6">
            <label for="name" class="block mb-2 text-sm font-medium leading-5 text-gray-700 sm:mt-px sm:pt-2">
                Search
            </label>
            <div class="max-w-2xl mt-1 sm:mt-0 sm:col-span-2">
                <input value="{{ $search_st }}" type="text" name="search_st" id="search_st" class="py-2 px-3 border border-gray-300 rounded block w-full transition duration-150 ease-in-out sm:text-sm sm:leading-5" />
            </div>
        </div>

        <div class="flex items-center">
            <button type="submit" class="mr-3 inline-flex items-center px-3 py-2 border border-transparent text-sm leading-4 font-medium rounded-md shadow-sm text-white bg-indigo-600 sm:leading-5 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                Filter
            </button>

            <div class="text-indigo-600 underline text-sm">
                <a href="/admin/questions/history">Clear filters</a>
            </div>
        </div>

    </form>


    @if($questions->isEmpty())
        <div class="flex items-center text-orange-600">
            <svg class="h-6 w-6 mr-2" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 9v2m0 4h.01m-6.938 4h13.856c1.54 0 2.502-1.667 1.732-3L13.732 4c-.77-1.333-2.694-1.333-3.464 0L3.34 16c-.77 1.333.192 3 1.732 3z" />
            </svg>

            <div>
                No questions found.
            </div>
        </div>
    @else

        <div class="flex flex-col mt-2">
            <div class="align-middle min-w-full overflow-x-auto shadow overflow-hidden sm:rounded-lg">
                <table class="min-w-full divide-y divide-gray-200">
                    <thead>
                    <tr>
                        <th class="whitespace-nowrap px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                Question
                        </th>
                        <th class="whitespace-nowrap px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                            User
                        </th>
                        <th class="whitespace-nowrap px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                            Is Answered?
                        </th>
                        <th class="whitespace-nowrap px-6 py-3 bg-gray-50 text-xs font-medium text-gray-500 uppercase tracking-wider">
                            &nbsp;
                        </th>
                    </tr>
                    </thead>
                    <tbody class="bg-white divide-y divide-gray-200">

                    @foreach($questions as $question)
                        <tr class="bg-white">
                            <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-900">
                                <div>
                                    {!! $question->trixRichText->first()->content !!}
                                </div>
                            </td>
                            <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-900">
                                <div class="text-gray-500">
                                    <div>
                                        {{ $question->user->name }}
                                    </div>
                                    <div class="text-sm text-gray-500">
                                        {{ $question->user->email }}
                                    </div>
                                </div>
                            </td>
                            <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-900">
                                @if(empty($question->answer_date))
                                    -
                                @else
                                    <svg xmlns="http://www.w3.org/2000/svg" class="text-lime-500 h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z" />
                                    </svg>
                                @endif
                            </td>
                            <td class="px-6 py-4 text-right whitespace-nowrap text-sm">
                                @if(empty($question->answer_date))
                                    <a href="{{ route('admin.questions.answer', $question->id) }}" class="text-indigo-600 hover:text-indigo-900">Answer</a>&nbsp;&nbsp;
                                @else
                                    <a href="{{ route('admin.questions.view', $question->id) }}" class="text-indigo-600 hover:text-indigo-900">View</a>&nbsp;&nbsp;
                                @endif
                            </td>
                        </tr>
                    @endforeach

                </tbody>
            </table>
        </div>

            <div class="mt-3">
                {{ $questions->links() }}
            </div>
    </div>

@endif
@endsection
