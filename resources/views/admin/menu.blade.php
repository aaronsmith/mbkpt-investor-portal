<a href="{{ route('admin.dashboard') }}" class="@if(request()->routeIs('admin.dashboard')) bg-black text-white @endif group flex items-center px-2 py-2 text-sm leading-6 font-medium rounded-md text-orange-100">
    <svg class="mr-4 h-6 w-6 text-gray-200" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 17v-2m3 2v-4m3 4v-6m2 10H7a2 2 0 01-2-2V5a2 2 0 012-2h5.586a1 1 0 01.707.293l5.414 5.414a1 1 0 01.293.707V19a2 2 0 01-2 2z" />
    </svg>
    Documents
</a>
<a href="{{ route('admin.users') }}" class="@if(request()->routeIs('admin.users') || request()->routeIs('admin.users.edit') || request()->routeIs('admin.users.history')) bg-black text-white @endif group flex items-center px-2 py-2 text-sm leading-6 font-medium rounded-md text-orange-100 hover:text-white hover:bg-gray-900">

    <svg class="mr-4 h-6 w-6 text-gray-200" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 4.354a4 4 0 110 5.292M15 21H3v-1a6 6 0 0112 0v1zm0 0h6v-1a6 6 0 00-9-5.197M13 7a4 4 0 11-8 0 4 4 0 018 0z" />
    </svg>
    Users
</a>
<a href="{{ route('admin.requests') }}" class="@if(request()->routeIs('admin.requests')) bg-black text-white @endif group flex items-center px-2 py-2 text-sm leading-6 font-medium rounded-md text-orange-100 hover:text-white hover:bg-gray-900">
    <svg class="mr-4 h-6 w-6 text-gray-200" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M18 9v3m0 0v3m0-3h3m-3 0h-3m-2-5a4 4 0 11-8 0 4 4 0 018 0zM3 20a6 6 0 0112 0v1H3v-1z" />
    </svg>
    Requests
</a>
<a href="{{ route('admin.faq') }}" class="@if(request()->routeIs('admin.faq')) bg-black text-white @endif group flex items-center px-2 py-2 text-sm leading-6 font-medium rounded-md text-orange-100 hover:text-white hover:bg-gray-900">
    <svg xmlns="http://www.w3.org/2000/svg" class="mr-4 h-6 w-6 text-gray-200" fill="none" viewBox="0 0 24 24" stroke="currentColor">
        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9.663 17h4.673M12 3v1m6.364 1.636l-.707.707M21 12h-1M4 12H3m3.343-5.657l-.707-.707m2.828 9.9a5 5 0 117.072 0l-.548.547A3.374 3.374 0 0014 18.469V19a2 2 0 11-4 0v-.531c0-.895-.356-1.754-.988-2.386l-.548-.547z" />
    </svg>
    FAQ
</a>
<a href="{{ route('admin.questions') }}" class="@if(request()->routeIs('admin.questions*')) bg-black text-white @endif group flex items-center px-2 py-2 text-sm leading-6 font-medium rounded-md text-orange-100 hover:text-white hover:bg-gray-900">
    <svg xmlns="http://www.w3.org/2000/svg" class="mr-4 h-6 w-6 text-gray-200" fill="none" viewBox="0 0 24 24" stroke="currentColor">
        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17 8h2a2 2 0 012 2v6a2 2 0 01-2 2h-2v4l-4-4H9a1.994 1.994 0 01-1.414-.586m0 0L11 14h4a2 2 0 002-2V6a2 2 0 00-2-2H5a2 2 0 00-2 2v6a2 2 0 002 2h2v4l.586-.586z" />
    </svg>
    User Questions

    @if( \App\Models\UserQuestion::whereNull('answer_date')->count() > 0)
    <span class="inline-flex ml-2 items-center px-3 py-0.5 rounded-full text-sm font-medium bg-red-100 text-red-800">
        {{ \App\Models\UserQuestion::whereNull('answer_date')->count() }}
    </span>
    @endif
</a>
<a href="{{ route('admin.roles') }}" class="@if(request()->routeIs('admin.roles')) bg-black text-white @endif group flex items-center px-2 py-2 text-sm leading-6 font-medium rounded-md text-orange-100 hover:text-white hover:bg-gray-900">
    <svg class="mr-4 h-6 w-6 text-gray-200" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 15v2m-6 4h12a2 2 0 002-2v-6a2 2 0 00-2-2H6a2 2 0 00-2 2v6a2 2 0 002 2zm10-10V7a4 4 0 00-8 0v4h8z" />
    </svg>
    Roles and Permissions
</a>
