@extends('_layouts.admin')

@section('content')

    <form action="{{ route('admin.faq.update', $faq->id) }}" method="post" class="">
        @csrf

        <div class="font-semibold text-lg mb-4">Edit Question</div>


        @if ($errors->any())
            <div class="bg-red-100 text-red-700 border border-red-200 p-4 text-sm mb-6 rounded">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif


        <div class="mb-4">
            <label class="block text-sm font-medium text-gray-700">
                Question
            </label>
            <div class="mt-1">
                <input value="{{ $faq->question }}" autocomplete="off" name="question" type="text" class="apnumber appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-lime-500 focus:border-lime-500 sm:text-sm">
            </div>
        </div>

        <div class="mb-4">
            <label class="block text-sm font-medium text-gray-700">
                Answer
            </label>
            <div class="mt-1">
                @trix($faq, 'content', [ 'hideButtonIcons' => ['heading-1', 'attach', 'quote', 'code'] ])
            </div>
        </div>

        <div class="mb-3">
            <label class="block text-sm font-medium text-gray-700">
                Position
            </label>
            <div class="mt-1">
                <input value="{{ $faq->position }}" autocomplete="off" name="position" type="text" class="apnumber max-w-xs appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-lime-500 focus:border-lime-500 sm:text-sm">
            </div>
        </div>

        <div>
            <button id="button" type="submit" class="flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-indigo-500 hover:bg-indigo-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                Save
            </button>
        </div>
    </form>

@endsection
