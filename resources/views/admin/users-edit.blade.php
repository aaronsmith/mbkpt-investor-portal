@extends('_layouts.admin')

@section('content')


    <h2 class="mt-6 mb-5 text-lg leading-6 font-medium text-gray-900">
        Edit User
    </h2>



    <form action="{{ route('admin.users.update', $user->id) }}" method="post" style="max-width: 500px;">
        @if ($errors->any())
            <div class="bg-red-100 text-red-700 border border-red-200 p-4 text-sm mb-6 rounded">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @csrf
        <div class="mb-3">
            <label for="email" class="mb-1 block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2">
                Name
            </label>
            <div class="mt-1 sm:mt-0 sm:col-span-2">
                <input value="{{ $user->name }}" id="name" name="name" type="text" autocomplete="off" class="block w-full shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm border-gray-300 rounded-md">
            </div>
        </div>

        <div class="mb-3">
            <label for="email" class="mb-1 block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2">
                Email address
            </label>
            <div class="mt-1 sm:mt-0 sm:col-span-2">
                <input value="{{ $user->email }}" id="email" name="email" type="email" autocomplete="off" class="block w-full shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm border-gray-300 rounded-md">
            </div>
        </div>

        <div class="mb-3 flex">
            <div>
                <label for="off" class="mb-1 block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2">
                    Div
                </label>
                <div class="mt-1 sm:mt-0 sm:col-span-2">
                    <input value="{{ $user->div }}" id="div" name="div" type="text" autocomplete="off" class="block w-full shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm border-gray-300 rounded-md">
                </div>
            </div>

            <div class="ml-3">
                <label for="off" class="mb-1 block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2">
                    INumber
                </label>
                <div class="mt-1 sm:mt-0 sm:col-span-2">
                    <input value="{{ $user->inumber }}" id="inumber" name="inumber" type="text" autocomplete="off" class="block w-full shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm border-gray-300 rounded-md">
                </div>
            </div>

            <div class="ml-3">
                <label for="off" class="mb-1 block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2">
                    AP number
                </label>
                <div class="mt-1 sm:mt-0 sm:col-span-2">
                    <input value="{{ $user->apnumber }}" id="ap_number" name="ap_number" type="text" autocomplete="off" class="block w-full shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm border-gray-300 rounded-md">
                </div>
            </div>
        </div>

        <div class="mb-3">
            <label for="off" class="mb-1 block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2">
                Address
            </label>
            <div class="mt-1 sm:mt-0 sm:col-span-2">
                <input value="{{ $user->address }}" id="address" name="address" type="text" autocomplete="off" class="block w-full shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm border-gray-300 rounded-md">
            </div>
        </div>

        <div class="mb-5 flex">
            <div>
                <label for="off" class="mb-1 block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2">
                    City
                </label>
                <div class="mt-1 sm:mt-0 sm:col-span-2">
                    <input value="{{ $user->city }}" id="city" name="city" type="text" autocomplete="off" class="block w-full shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm border-gray-300 rounded-md">
                </div>
            </div>

            <div class="ml-3">
                <label for="off" class="mb-1 block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2">
                    State
                </label>
                <div class="mt-1 sm:mt-0 sm:col-span-2">
                    <input value="{{ $user->state }}" id="state" name="state" type="text" autocomplete="off" class="block w-full shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm border-gray-300 rounded-md">
                </div>
            </div>

            <div class="ml-3">
                <label for="off" class="mb-1 block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2">
                    Zip code
                </label>
                <div class="mt-1 sm:mt-0 sm:col-span-2">
                    <input value="{{ $user->zip }}" id="zip" name="zip" type="text" autocomplete="off" class="block w-full shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm border-gray-300 rounded-md">
                </div>
            </div>

            <div class="ml-3">
                <label for="off" class="mb-1 block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2">
                    Country
                </label>
                <div class="mt-1 sm:mt-0 sm:col-span-2">
                    <input value="{{ $user->country }}" id="country" name="country" type="text" autocomplete="off" class="block w-full shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm border-gray-300 rounded-md">
                </div>
            </div>
        </div>

        <label for="off" class="mb-2 block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2">
            Roles
        </label>

        @foreach($roles as $role)
            <label class="relative mb-2 flex cursor-pointer">
                <input type="checkbox" @if($user->hasRole($role->name)) checked @endif name="roles[]" value="{{ $role->id }}" class="h-4 w-4 mt-0.5 cursor-pointer text-indigo-600 border-gray-300 focus:ring-indigo-500" aria-labelledby="privacy-setting-1-label" aria-describedby="privacy-setting-1-description">
                <div class="ml-3 flex flex-col">
                    <!-- Checked: "text-indigo-900", Not Checked: "text-gray-900" -->
                    <span id="privacy-setting-1-label" class="text-gray-900 block text-sm font-medium">
                      {{ $role->name }}
                    </span>
                </div>
            </label>
        @endforeach

        <div class="flex mt-6">
            <a href="{{ URL::previous() }}" class="mr-2 inline-flex items-center px-3 py-2 border border-gray-300 shadow-sm text-sm leading-4 font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                Back
            </a>

            <button type="submit" class="inline-flex items-center px-3 py-2 border border-transparent text-sm leading-4 font-medium rounded-md shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                Update
            </button>
        </div>

        @if(request()->has('return'))
            <input type="hidden" name="return" value="{{ request()->get('return') }}">
        @endif
    </form>
@endsection
