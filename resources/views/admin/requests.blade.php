@extends('_layouts.admin')

@section('content')
    @if($requests->isEmpty())
        <div class="flex items-center text-orange-600">
            <svg class="h-6 w-6 mr-2" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 9v2m0 4h.01m-6.938 4h13.856c1.54 0 2.502-1.667 1.732-3L13.732 4c-.77-1.333-2.694-1.333-3.464 0L3.34 16c-.77 1.333.192 3 1.732 3z" />
            </svg>

            <div>
                No requests found.
            </div>
        </div>
    @else

        <form action="{{ route('admin.requests.generate-mass') }}" method="post">
            @csrf
            <div class="flex flex-col mt-2">
                <div class="align-middle min-w-full overflow-x-auto shadow overflow-hidden sm:rounded-lg">
                    <table class="min-w-full divide-y divide-gray-200">
                        <thead>
                        <tr>
                            <th class="whitespace-nowrap px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">&nbsp;</th>
                            <th class="whitespace-nowrap px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                Name / Email
                            </th>
                            <th class="whitespace-nowrap px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                Investor number
                            </th>
                            <th class="whitespace-nowrap px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                AP number
                            </th>
                            <th class="whitespace-nowrap px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                Zip code
                            </th>
                            <th class="whitespace-nowrap px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                Request date
                            </th>
                            <th class="whitespace-nowrap px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                &nbsp;Last Downloaded
                            </th>
                            <th class="whitespace-nowrap px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody class="bg-white divide-y divide-gray-200">

                        @foreach($requests as $req)
                            <tr class="bg-white">
                                <td class="max-w-0 px-6 py-4 whitespace-nowrap text-sm text-center text-gray-900">
                                    <input onclick="update();" id="comments" name="requests[]" value="{{ $req->id }}" type="checkbox" class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded">
                                </td>
                                <td class="max-w-0 w-full px-6 py-4 whitespace-nowrap text-sm text-gray-900">
                                    <div>
                                        {{ $req->name }}
                                    </div>

                                    <div class="text-gray-500 text-xs">{{ $req->email }}</div>
                                </td>
                                <td class="max-w-0 w-full px-6 py-4 whitespace-nowrap text-sm text-gray-900">
                                    {{ $req->investor_number }}
                                </td>
                                <td class="max-w-0 w-full px-6 py-4 whitespace-nowrap text-sm text-gray-900">
                                    {{ $req->apnumber }}
                                </td>
                                <td class="max-w-0 w-full px-6 py-4 whitespace-nowrap text-sm text-gray-900">
                                    {{ $req->zip }}
                                </td>
                                <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                    {{ $req->request_date->format("m-d-Y") }}
                                </td>
                                <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                    @if($req->logs()->where('log_item_id', 15)->exists())
                                        <div class="flex items-center">
                                            <svg class="h-5 w-5 text-lime-600 mr-2" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 13l4 4L19 7" />
                                            </svg>

                                            <div>
                                                {{ $req->logs()->where('log_item_id', 15)->first()->created_at->format("m-d-Y") }}
                                            </div>
                                        </div>
                                    @else
                                        &nbsp;
                                    @endif
                                </td>
                                <td class="px-6 py-4 text-right whitespace-nowrap text-sm">
                                    <a href="{{ route('admin.users.edit', [$req->id, 'return' => 'requests']) }}" class="text-indigo-600 font-medium underline">View / Edit</a>&nbsp;&nbsp;
                                    <a href="{{ route('admin.users.delete', $req->id) }}" class="text-red-600 font-medium underline">Deny</a>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>

            <div class="mt-3">
                <button id="button" type="submit" disabled="disabled" class="disabled:cursor-not-allowed disabled:opacity-50 flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-indigo-500 hover:bg-indigo-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                    Download Documents
                </button>
            </div>
        </form>


    @endif


    <script>
        function update(){
            let total = document.querySelectorAll('input[type="checkbox"]:checked').length
            document.getElementById('button').disabled = true;

            if(total > 0){
                document.getElementById('button').disabled = false;
            }
        }
    </script>
@endsection
