@extends('_layouts.admin')

@section('content')
    <h2 class="mt-8 mb-6 text-lg leading-6 font-medium text-gray-900">
        History for {{ $user->name }}
    </h2>

    <div class="mb-6">
        <a class="underline text-lime-500" href="{{ route('admin.users') }}">Back</a>
    </div>

    <!-- This example requires Tailwind CSS v2.0+ -->
    <div class="flow-root max-w-lg">
        <ul class="-mb-8 border-t border-gray-200">
            @foreach($user->logs()->orderBy('id', 'desc')->get() as $log)
            <li class="relative flex py-3 border-b border-gray-200">
                <div class="min-w-0 flex-1 pt-1.5 flex justify-between space-x-4">
                    <div>
                        <p class="text-sm text-gray-800">{{ $log->item->name }}</p>
                    </div>

                    <div class="text-right text-sm whitespace-nowrap text-gray-500">
                        <time datetime="2020-10-04">{{ $log->created_at->format("d-m-Y H:i") }}</time>
                        @if($log->ip)
                            <p class="text-xs text-gray-800">From {{ $log->ip }}</p>
                        @endif
                    </div>
                </div>
            </li>
            @endforeach
        </ul>
    </div>

@endsection
