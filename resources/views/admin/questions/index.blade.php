@extends('_layouts.admin')

@section('content')
    @if($questions->isEmpty())
        <div class="flex items-center text-orange-600">
            <svg class="h-6 w-6 mr-2" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 9v2m0 4h.01m-6.938 4h13.856c1.54 0 2.502-1.667 1.732-3L13.732 4c-.77-1.333-2.694-1.333-3.464 0L3.34 16c-.77 1.333.192 3 1.732 3z" />
            </svg>

            <div>
                No pending questions found.
            </div>
        </div>
    @else

        <!-- This example requires Tailwind CSS v2.0+ -->
        <div class="flex flex-col">
            <div class="-my-2 overflow-x-auto">
                <div class="py-2 align-middle inline-block min-w-full">
                    <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                        <table class="min-w-full divide-y divide-gray-200">
                            <thead class="bg-gray-50">

                            <tr>
                                <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    Question
                                </th>
                                <th scope="col" class="relative px-6 py-3">
                                    <span class="sr-only">Edit</span>
                                </th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($questions as $question)
                            <tr class="border-b border-gray-200 @if($loop->index % 2 == 0) bg-white @else bg-gray-50 @endif">
                                <td class="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                                    <div class="text-sm">{{ $question->user->name }}</div>
                                    <div class="mb-2 text-gray-600 text-xs">{{ $question->created_at->format("m-d-Y H:i") }}</div>
                                    <p>
                                        {!! $question->trixRichText->first()->content !!}
                                    </p>
                                </td>
                                <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                                    <a href="{{ route('admin.questions.answer', $question->id) }}" class="text-indigo-600 hover:text-indigo-900">Answer</a>&nbsp;&nbsp;
                                </td>
                            </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    @endif

    <div class="mt-4">
        <a href="{{ route('admin.questions.history') }}" class="underline text-lime-600">Question History</a>
    </div>

@endsection
