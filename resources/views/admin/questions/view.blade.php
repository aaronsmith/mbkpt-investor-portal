@extends('_layouts.admin')

@section('content')

    <div class="mb-10 bg-white shadow rounded">
        <div class="mb-2 p-4 border-b border-gray-200 flex items-center justify-between">
            <div class="font-semibold text-lg">Question</div>
            <div>
                <div class="text-sm text-gray-600">By {{ $question->user->name }} on {{ $question->created_at->format("m-d-Y H:i") }}</div>
                <div class="text-xs text-gray-600">{{ $question->user->email }}</div>
            </div>
        </div>

        <div class="mt-1 p-4">
            {!! $question->trixRichText->first()->content !!}
        </div>
    </div>

    <div class="mb-10 bg-white shadow rounded">
        <div class="mb-2 p-4 border-b border-gray-200 flex items-center justify-between">
            <div class="font-semibold text-lg">Answer</div>
            <div>
                <div class="text-sm text-gray-600">By {{ $question->admin->name }} on {{ $question->answer_date->format("m-d-Y H:i") }}</div>
                <div class="text-xs text-gray-600">{{ $question->admin->email }}</div>
            </div>
        </div>
        <div class="mt-1 p-4">
            {!! $question->trixRichText->where('field', 'answer')->first()->content !!}
        </div>
    </div>

    <div>
        <a href="{{ URL::previous() }}" class="text-lime-600 text-sm underline">Back to questions</a>
    </div>

@endsection
