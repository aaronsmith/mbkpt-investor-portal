@extends('_layouts.admin')

@section('content')

    <form action="{{ route('admin.questions.answer.save', $question->id) }}" method="post" class="">
        @csrf
        <div class="font-semibold text-lg mb-4">Answer a Question</div>

        <div class="mb-10">
            <div class="text-sm text-gray-600">By {{ $question->user->name }} on {{ $question->created_at->format("m-d-Y H:i") }}</div>
            <div class="mt-1">
                {!! $question->trixRichText->first()->content !!}
            </div>
        </div>


        <div class="mb-4">
            <label class="block text-sm font-medium text-gray-700">
                Answer
            </label>
            <div class="mt-1">
                @trix(\App\Models\UserQuestion::class, 'answer', [ 'hideButtonIcons' => ['heading-1', 'attach', 'quote', 'code'] ])
            </div>
        </div>

        <div>
            <button id="button" type="submit" class="flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-indigo-500 hover:bg-indigo-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                Save
            </button>
        </div>
    </form>

@endsection
