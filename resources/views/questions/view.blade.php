<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Trustee Registration Portal</title>

    <link rel="stylesheet" href="https://rsms.me/inter/inter.css">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @trixassets
</head>
<body class="antialiased">
<!--
This example requires Tailwind CSS v2.0+

This example requires some changes to your config:

```
// tailwind.config.js
const colors = require('tailwindcss/colors')

module.exports = {
// ...
theme: {
  extend: {
    colors: {
      lime: colors.lime,
    }
  }
},
plugins: [
  // ...
  require('@tailwindcss/forms'),
]
}
```
-->
<div class="h-screen flex overflow-hidden bg-gray-100">
    <div class="hidden lg:flex lg:flex-shrink-0">
        <div class="flex flex-col w-64">
            <!-- Sidebar component, swap this element with another sidebar if you like -->
            <div class="flex flex-col flex-grow bg-lime-700 pt-5 pb-4 overflow-y-auto">
                @include('_menu')
            </div>
        </div>
    </div>

    <div class="flex-1 overflow-auto focus:outline-none" tabindex="0">

        <main class="flex-1 relative pb-8 z-0 overflow-y-auto">
            <!-- Page header -->
            <div class="bg-white shadow">
                <div class="px-4 sm:px-6 lg:max-w-6xl lg:mx-auto lg:px-8">
                    <div class="py-6 md:flex md:items-center md:justify-between lg:border-t lg:border-gray-200">
                        <div class="flex-1 min-w-0">
                            <!-- Profile -->
                            <div class="flex items-center justify-between">
                                <div>
                                    <img src="{{ asset('images/logo.jpg') }}" alt="">
                                </div>

                                <div class="flex items-center">
                                        <span class="inline-flex items-center justify-center h-14 w-14 rounded-full bg-gray-500">
                                          <span class="text-xl font-medium leading-none text-white">{{ Auth::user()->initials }}</span>
                                        </span>

                                    <div>
                                        <div class="flex items-center">
                                            <h1 class="ml-3 text-2xl font-light leading-7 text-gray-700 sm:leading-7 sm:truncate">
                                                {{ Auth::user()->name }}
                                            </h1>
                                        </div>
                                        <dl class="mt-6 flex flex-col sm:ml-3 sm:mt-1 sm:flex-row sm:flex-wrap">
                                            <dd class="mt-3 flex items-center text-sm text-gray-500 font-medium sm:mr-6 sm:mt-0 capitalize">
                                                Investor number {{ Auth::user()->investor_number }}
                                            </dd>
                                        </dl>
                                    </div>


                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>

            @if(Auth::user()->isAbleTo('Download Financial Record'))
                <a class="shadow block bg-gray-700 p-3 text-white text-center" href="{{ route('dashboard') }}">
                    Click here for access to annual Trust Accountings from the inception of the Trust, including the Trust’s calendar-year-end Historical Balance Sheets and Trust Operations Reports.
                </a>
            @endif

            <div class="p-6">
                <div class="mb-10 bg-white shadow rounded">
                    <div class="mb-2 p-4 border-b border-gray-200 flex items-center justify-between">
                        <div class="font-semibold text-lg">Question</div>
                        <div>
                            <div class="text-sm text-gray-600">By {{ $question->user->name }} on {{ $question->created_at->format("m-d-Y H:i") }}</div>
                            <div class="text-xs text-gray-600">{{ $question->user->email }}</div>
                        </div>
                    </div>

                    <div class="mt-1 p-4">
                        {!! $question->trixRichText->first()->content !!}
                    </div>
                </div>

                <div class="mb-10 bg-white shadow rounded">
                    <div class="mb-2 p-4 border-b border-gray-200 flex items-center justify-between">
                        <div class="font-semibold text-lg">Answer</div>
                        <div>
                            <div class="text-sm text-gray-600">By {{ $question->admin->name }} on {{ $question->answer_date->format("m-d-Y H:i") }}</div>
                            <div class="text-xs text-gray-600">{{ $question->admin->email }}</div>
                        </div>
                    </div>
                    <div class="mt-1 p-4">
                        {!! $question->trixRichText->where('field', 'answer')->first()->content !!}
                    </div>
                </div>

                <div>
                    <a href="{{ route('questions') }}" class="text-lime-600 text-sm underline">Back to questions</a>
                </div>
            </div>
        </main>
    </div>
</div>


</body>
</html>

