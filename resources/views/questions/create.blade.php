<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Trustee Registration Portal</title>

    <link rel="stylesheet" href="https://rsms.me/inter/inter.css">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @trixassets
</head>
<body class="antialiased">
<!--
This example requires Tailwind CSS v2.0+

This example requires some changes to your config:

```
// tailwind.config.js
const colors = require('tailwindcss/colors')

module.exports = {
// ...
theme: {
  extend: {
    colors: {
      lime: colors.lime,
    }
  }
},
plugins: [
  // ...
  require('@tailwindcss/forms'),
]
}
```
-->
<div class="h-screen flex overflow-hidden bg-gray-100">
    <div class="hidden lg:flex lg:flex-shrink-0">
        <div class="flex flex-col w-64">
            <!-- Sidebar component, swap this element with another sidebar if you like -->
            <div class="flex flex-col flex-grow bg-lime-700 pt-5 pb-4 overflow-y-auto">
                @include('_menu')
            </div>
        </div>
    </div>

    <div class="flex-1 overflow-auto focus:outline-none" tabindex="0">

        <main class="flex-1 relative pb-8 z-0 overflow-y-auto">
            <!-- Page header -->
            <div class="bg-white shadow">
                <div class="px-4 sm:px-6 lg:max-w-6xl lg:mx-auto lg:px-8">
                    <div class="py-6 md:flex md:items-center md:justify-between lg:border-t lg:border-gray-200">
                        <div class="flex-1 min-w-0">
                            <!-- Profile -->
                            <div class="flex items-center justify-between">
                                <div>
                                    <img src="{{ asset('images/logo.jpg') }}" alt="">
                                </div>

                                <div class="flex items-center">
                                        <span class="inline-flex items-center justify-center h-14 w-14 rounded-full bg-gray-500">
                                          <span class="text-xl font-medium leading-none text-white">{{ Auth::user()->initials }}</span>
                                        </span>

                                    <div>
                                        <div class="flex items-center">
                                            <h1 class="ml-3 text-2xl font-light leading-7 text-gray-700 sm:leading-7 sm:truncate">
                                                {{ Auth::user()->name }}
                                            </h1>
                                        </div>
                                        <dl class="mt-6 flex flex-col sm:ml-3 sm:mt-1 sm:flex-row sm:flex-wrap">
                                            <dd class="mt-3 flex items-center text-sm text-gray-500 font-medium sm:mr-6 sm:mt-0 capitalize">
                                                Investor number {{ Auth::user()->investor_number }}
                                            </dd>
                                        </dl>
                                    </div>


                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>

            @if(Auth::user()->isAbleTo('Download Financial Record'))
                <a class="shadow block bg-gray-700 p-3 text-white text-center" href="{{ route('dashboard') }}">
                    Click here for access to annual Trust Accountings from the inception of the Trust, including the Trust’s calendar-year-end Historical Balance Sheets and Trust Operations Reports.
                </a>
            @endif

            <div class="p-6">
                <h1 class="text-lime-600 text-3xl font-semibold mb-4">My Questions</h1>

                <form action="{{ route('questions.save') }}" method="post" class="">
                    @csrf
                    <div class="font-semibold text-lg mb-4">Add a Question</div>

                    @if ($errors->any())
                        <div class="bg-red-100 text-red-700 border border-red-200 p-4 text-sm mb-6 rounded">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="mb-4">
                        <div class="mt-1">
                            @trix(\App\Models\UserQuestion::class, 'content', [ 'hideButtonIcons' => ['heading-1', 'attach', 'quote', 'code'] ])
                        </div>
                    </div>

                    <label class="relative mb-2 flex cursor-pointer">
                        <input type="checkbox" name="already_checked_faq" value="1" class="h-4 w-4 mt-0.5 cursor-pointer text-indigo-600 border-gray-300 focus:ring-indigo-500" aria-labelledby="privacy-setting-1-label" aria-describedby="privacy-setting-1-description">
                        <div class="ml-3 flex flex-col">
                            <!-- Checked: "text-indigo-900", Not Checked: "text-gray-900" -->
                            <span id="privacy-setting-1-label" class="text-gray-900 block text-sm font-medium">
                          I have first checked the <a class="text-lime-600 underline" href="{{ route('faq-auth') }}">frequently asked questions list</a>.
                        </span>
                        </div>
                    </label>

                    <div class="mt-4">
                        <button class="inline-block justify-center py-2 px-4 border border-transparent shadow-lg text-sm font-medium text-white bg-lime-500 hover:bg-lime-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-lime-500">
                            Save
                        </button>
                    </div>
                </form>
            </div>
        </main>
    </div>
</div>


</body>
</html>
