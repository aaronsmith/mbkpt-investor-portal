@extends('_layouts.main')

@section('content')
    @if($questions->isEmpty())
        <div class="flex items-center text-orange-600">
            <svg class="h-6 w-6 mr-2" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 9v2m0 4h.01m-6.938 4h13.856c1.54 0 2.502-1.667 1.732-3L13.732 4c-.77-1.333-2.694-1.333-3.464 0L3.34 16c-.77 1.333.192 3 1.732 3z" />
            </svg>

            <div>
                No questions found.
            </div>
        </div>
    @else

        <!--
  This example requires Tailwind CSS v2.0+

  This example requires some changes to your config:

  ```
  // tailwind.config.js
  const colors = require('tailwindcss/colors')

  module.exports = {
    // ...
    theme: {
      extend: {
        colors: {
          'light-blue': colors.lightBlue,
          teal: colors.teal,
          rose: colors.rose,
        }
      }
    }
  }
  ```
-->

        <div>

            <h1 class="text-xl font-semibold text-lime-700">Frequently Asked Questions</h1>

            <div class="flow-root mt-6">
                <ul>
                    @foreach($questions as $question)
                        <li class="shadow rounded mb-6">
                            <div class="relative focus-within:ring-2 focus-within:ring-indigo-500">
                                <h3 class="p-4 text bg-gray-100 font-semibold border-b border-gray-200 text-gray-800">
                                    {{ $question->question }}
                                </h3>
                                <div class="p-4 text-sm text-gray-500">
                                    {!! $question->trixRichText->first()->content !!}
                                </div>
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>

        <div class="mt-4">
            <a class="underline text-lime-600" href="{{ route('questions') }}">If you still have questions, please click here to login.</a>
        </div>
        <div class="mt-4 text-right">
            <a class="underline text-gray-400 text-right inline-block text-sm" href="{{ route('winddown') }}">Go back to Wind down information.</a>
        </div>

    @endif
@endsection

