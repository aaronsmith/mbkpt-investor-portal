<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Trustee Registration Portal</title>

        <link rel="stylesheet" href="https://rsms.me/inter/inter.css">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    </head>
    <body class="antialiased">
    <!--
  This example requires Tailwind CSS v2.0+

  This example requires some changes to your config:

  ```
  // tailwind.config.js
  const colors = require('tailwindcss/colors')

  module.exports = {
    // ...
    theme: {
      extend: {
        colors: {
          lime: colors.lime,
        }
      }
    },
    plugins: [
      // ...
      require('@tailwindcss/forms'),
    ]
  }
  ```
-->
    <!-- This example requires Tailwind CSS v2.0+ -->
    <div class="fixed z-10 inset-0 overflow-y-auto hidden" id="modal" aria-labelledby="modal-title" role="dialog" aria-modal="true">
        <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
            <!--
              Background overlay, show/hide based on modal state.

              Entering: "ease-out duration-300"
                From: "opacity-0"
                To: "opacity-100"
              Leaving: "ease-in duration-200"
                From: "opacity-100"
                To: "opacity-0"
            -->
            <div class="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" aria-hidden="true"></div>

            <!-- This element is to trick the browser into centering the modal contents. -->
            <span class="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">&#8203;</span>

            <!--
              Modal panel, show/hide based on modal state.

              Entering: "ease-out duration-300"
                From: "opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                To: "opacity-100 translate-y-0 sm:scale-100"
              Leaving: "ease-in duration-200"
                From: "opacity-100 translate-y-0 sm:scale-100"
                To: "opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            -->
            <div class="inline-block align-bottom bg-white rounded-lg px-4 pt-5 pb-4 text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full sm:p-6">
                <div>
                    <div class="mt-3 text-center sm:mt-5">
                        <h3 class="text-lg leading-6 font-medium text-gray-900" id="modal-title">
                            Important
                        </h3>
                    </div>
                </div>
                <div>
                    <div class="relative flex mt-4">
                        <div class="flex items-center h-5">
                            <input onclick="validate()" id="consent" name="comments" type="checkbox" class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-500 rounded">
                        </div>
                        <div class="ml-3 text-sm">
                            <label for="comments" class="font-medium text-gray-700">By checking this box, I acknowledge that I am a Keep Policy Investor in accordance with the Mutual Benefits Keep Policy Trust dated September 25, 2009.</label>
                        </div>
                    </div>
                </div>
                <div class="mt-5 sm:mt-6 sm:grid sm:grid-cols-2 sm:gap-3 sm:grid-flow-row-dense">
                    <a href="#" type="button" id="consent-button" class="opacity-50 w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4 py-2 bg-lime-600 text-base font-medium text-white hover:bg-lime-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-lime-500 sm:col-start-2 sm:text-sm">
                        Download
                    </a>
                    <button onclick="hide()" type="button" class="mt-3 w-full inline-flex justify-center rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-base font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:mt-0 sm:col-start-1 sm:text-sm">
                        Cancel
                    </button>
                </div>
            </div>
        </div>
    </div>


    <div class="h-screen flex overflow-hidden bg-gray-100">
        <div class="hidden lg:flex lg:flex-shrink-0">
            <div class="flex flex-col w-64">
                <!-- Sidebar component, swap this element with another sidebar if you like -->
                <div class="flex flex-col flex-grow bg-lime-700 pt-5 pb-4 overflow-y-auto">
                    @include('_menu')
                </div>
            </div>
        </div>

        <div class="flex-1 overflow-auto focus:outline-none" tabindex="0">

            <main class="flex-1 relative pb-8 z-0 overflow-y-auto">
                <!-- Page header -->
                <div class="bg-white shadow">
                    <div class="px-4 sm:px-6 lg:max-w-6xl lg:mx-auto lg:px-8">
                        <div class="py-6 md:flex md:items-center md:justify-between lg:border-t lg:border-gray-200">
                            <div class="flex-1 min-w-0">
                                <!-- Profile -->
                                <div class="flex items-center justify-between">
                                    <div>
                                        <img src="{{ asset('images/logo.jpg') }}" alt="">
                                    </div>

                                    <div class="flex items-center">
                                        <span class="inline-flex items-center justify-center h-14 w-14 rounded-full bg-gray-500">
                                          <span class="text-xl font-medium leading-none text-white">{{ Auth::user()->initials }}</span>
                                        </span>

                                        <div>
                                            <div class="flex items-center">
                                                <h1 class="ml-3 text-2xl font-light leading-7 text-gray-700 sm:leading-7 sm:truncate">
                                                    {{ Auth::user()->name }}
                                                </h1>
                                            </div>
                                            <dl class="mt-6 flex flex-col sm:ml-3 sm:mt-1 sm:flex-row sm:flex-wrap">
                                                <dd class="mt-3 flex items-center text-sm text-gray-500 font-medium sm:mr-6 sm:mt-0 capitalize">
                                                    Investor number {{ Auth::user()->investor_number }}
                                                </dd>
                                            </dl>
                                        </div>


                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="p-6">
                    <h2 class="max-w-6xl  mt-8 mb-6 text-lg leading-6 font-medium text-gray-900">
                        Available documents
                    </h2>

                    @if(!Auth::user()->isAbleTo('Download Financial Record'))
                        <div class="flex items-center text-red-600">
                            <svg class="h-6 w-6 mr-2" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 9v2m0 4h.01m-6.938 4h13.856c1.54 0 2.502-1.667 1.732-3L13.732 4c-.77-1.333-2.694-1.333-3.464 0L3.34 16c-.77 1.333.192 3 1.732 3z" />
                            </svg>

                            <div>
                                You are not allowed to access documents at this moment.
                            </div>
                        </div>
                    @else
                        @if($files->isEmpty())
                            <div class="flex items-center text-red-600">
                                <svg class="h-6 w-6 mr-2" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 9v2m0 4h.01m-6.938 4h13.856c1.54 0 2.502-1.667 1.732-3L13.732 4c-.77-1.333-2.694-1.333-3.464 0L3.34 16c-.77 1.333.192 3 1.732 3z" />
                                </svg>

                                <div>
                                    No documents found.
                                </div>
                            </div>
                        @else

                            <div class="flex flex-col mt-2">
                                <div class="align-middle min-w-full overflow-x-auto shadow overflow-hidden sm:rounded-lg">
                                    <table class="min-w-full divide-y divide-gray-200">
                                        <thead>
                                        <tr>
                                            <th class="whitespace-nowrap px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                Document name
                                            </th>
                                            <th class="whitespace-nowrap px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                Document type
                                            </th>
                                            <th class="whitespace-nowrap px-6 py-3 bg-gray-50 text-right text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                Last updated
                                            </th>
                                            <th class="whitespace-nowrap px-6 py-3 bg-gray-50 text-right text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                &nbsp;
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody class="bg-white divide-y divide-gray-200">

                                        @foreach($files as $file)
                                            <tr class="bg-white">
                                                <td class="max-w-0 w-full px-6 py-4 whitespace-nowrap text-sm text-gray-900">
                                                    {{ $file->name }}
                                                </td>
                                                <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                                    {{ \Illuminate\Support\Str::upper($file->extension) }}
                                                </td>
                                                <td class="px-6 py-4 text-right whitespace-nowrap text-sm text-gray-500">
                                                    {{ $file->created_at->format("m-d-Y") }}
                                                </td>
                                                <td class="px-6 py-4 text-right whitespace-nowrap text-sm">
                                                    <a target="_blank" onclick="consent('{{ route('download', $file->id) }}')" class="cursor-pointer text-lime-600 font-medium underline">Download</a>
                                                </td>
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        @endif

                    @endif

                </div>
            </main>
        </div>
    </div>

    <script type="text/javascript">
        function consent(url){
            document.getElementById('modal').classList.remove('hidden');
            document.getElementById('consent-button').dataset.url = url;
        }

        function hide(){
            document.getElementById('modal').classList.add('hidden');
            document.getElementById("consent").checked = false;
            document.getElementById('consent-button').classList.remove('opacity-100');
            document.getElementById('consent-button').classList.add('opacity-50');
            document.getElementById('consent-button').href = "#"
        }

        function validate() {
            if(document.getElementById('consent').checked){
                document.getElementById('consent-button').href = document.getElementById('consent-button').dataset.url;
                document.getElementById('consent-button').classList.remove('opacity-50');
                document.getElementById('consent-button').classList.add('opacity-100');
            }else{
                document.getElementById('consent-button').href = "#"
                document.getElementById('consent-button').classList.remove('opacity-100');
                document.getElementById('consent-button').classList.add('opacity-50');
            }
        }
    </script>
    </body>
</html>
