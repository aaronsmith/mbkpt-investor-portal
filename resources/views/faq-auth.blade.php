<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Trustee Registration Portal</title>

    <link rel="stylesheet" href="https://rsms.me/inter/inter.css">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body class="antialiased">
<!--
This example requires Tailwind CSS v2.0+

This example requires some changes to your config:

```
// tailwind.config.js
const colors = require('tailwindcss/colors')

module.exports = {
// ...
theme: {
  extend: {
    colors: {
      lime: colors.lime,
    }
  }
},
plugins: [
  // ...
  require('@tailwindcss/forms'),
]
}
```
-->
<div class="h-screen flex overflow-hidden bg-gray-100">
    <div class="hidden lg:flex lg:flex-shrink-0">
        <div class="flex flex-col w-64">
            <!-- Sidebar component, swap this element with another sidebar if you like -->
            <div class="flex flex-col flex-grow bg-lime-700 pt-5 pb-4 overflow-y-auto">
                @include('_menu')
            </div>
        </div>
    </div>

    <div class="flex-1 overflow-auto focus:outline-none" tabindex="0">

        <main class="flex-1 relative pb-8 z-0 overflow-y-auto">
            <!-- Page header -->
            <div class="bg-white shadow">
                <div class="px-4 sm:px-6 lg:max-w-6xl lg:mx-auto lg:px-8">
                    <div class="py-6 md:flex md:items-center md:justify-between lg:border-t lg:border-gray-200">
                        <div class="flex-1 min-w-0">
                            <!-- Profile -->
                            <div class="flex items-center justify-between">
                                <div>
                                    <img src="{{ asset('images/logo.jpg') }}" alt="">
                                </div>

                                <div class="flex items-center">
                                        <span class="inline-flex items-center justify-center h-14 w-14 rounded-full bg-gray-500">
                                          <span class="text-xl font-medium leading-none text-white">{{ Auth::user()->initials }}</span>
                                        </span>

                                    <div>
                                        <div class="flex items-center">
                                            <h1 class="ml-3 text-2xl font-light leading-7 text-gray-700 sm:leading-7 sm:truncate">
                                                {{ Auth::user()->name }}
                                            </h1>
                                        </div>
                                        <dl class="mt-6 flex flex-col sm:ml-3 sm:mt-1 sm:flex-row sm:flex-wrap">
                                            <dd class="mt-3 flex items-center text-sm text-gray-500 font-medium sm:mr-6 sm:mt-0 capitalize">
                                                Investor number {{ Auth::user()->investor_number }}
                                            </dd>
                                        </dl>
                                    </div>


                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="p-6">
                @if($questions->isEmpty())
                    <div class="flex items-center text-orange-600">
                        <svg class="h-6 w-6 mr-2" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 9v2m0 4h.01m-6.938 4h13.856c1.54 0 2.502-1.667 1.732-3L13.732 4c-.77-1.333-2.694-1.333-3.464 0L3.34 16c-.77 1.333.192 3 1.732 3z" />
                        </svg>

                        <div>
                            No questions found.
                        </div>
                    </div>
                @else

                    <div>

                        <h1 class="text-xl font-semibold text-lime-700 mb-6">Frequently Asked Questions</h1>

                        <div class="">
                            <ul>
                                @foreach($questions as $question)
                                    <li class="shadow rounded mb-6 bg-white">
                                        <div class="relative focus-within:ring-2 focus-within:ring-indigo-500">
                                            <h3 class="p-4 text bg-gray-100 font-semibold border-b border-gray-200 text-gray-800">
                                                {{ $question->question }}
                                            </h3>
                                            <div class="p-4 text-sm text-gray-500">
                                                {!! $question->trixRichText->first()->content !!}
                                            </div>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>

                @endif

            </div>
        </main>
    </div>
</div>


</body>
</html>

