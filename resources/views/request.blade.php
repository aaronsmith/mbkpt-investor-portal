@extends('_layouts.main')

@section('content')
    <div class="sm:mx-auto sm:w-full sm:max-w-xl mb-10">
        <h2 class="mt-6 text-center text-3xl font-light text-gray-600">
            Request access to your account
        </h2>
    </div>

    <div class="sm:mx-auto sm:w-full sm:max-w-lg">

        @if ($errors->any())
            <div class="rounded-md bg-red-100 p-4 w-full mx-auto mb-6">
                <div class="flex">
                    <div class="flex-shrink-0">
                        <!-- Heroicon name: x-circle -->
                        <svg class="h-5 w-5 text-red-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                            <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zM8.707 7.293a1 1 0 00-1.414 1.414L8.586 10l-1.293 1.293a1 1 0 101.414 1.414L10 11.414l1.293 1.293a1 1 0 001.414-1.414L11.414 10l1.293-1.293a1 1 0 00-1.414-1.414L10 8.586 8.707 7.293z" clip-rule="evenodd" />
                        </svg>
                    </div>
                    <div class="ml-3">
                        <h3 class="font-medium text-red-800">
                            Your submission has some errors.
                        </h3>
                        <div class="mt-2 text-sm text-red-700">
                            <ul class="list-disc pl-5 space-y-1">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        @else
            @if(session()->has('request-ip-error'))
                <div class="rounded-md bg-red-100 p-4 w-full mx-auto mb-6">
                    <div class="flex">
                        <div class="flex-shrink-0">
                            <!-- Heroicon name: x-circle -->
                            <svg class="h-5 w-5 text-red-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zM8.707 7.293a1 1 0 00-1.414 1.414L8.586 10l-1.293 1.293a1 1 0 101.414 1.414L10 11.414l1.293 1.293a1 1 0 001.414-1.414L11.414 10l1.293-1.293a1 1 0 00-1.414-1.414L10 8.586 8.707 7.293z" clip-rule="evenodd" />
                            </svg>
                        </div>
                        <div class="ml-3">
                            <h3 class="font-medium text-red-800">
                                Your submission has some errors.
                            </h3>
                            <div class="mt-2 text-sm text-red-700">
                                <ul class="list-disc pl-5 space-y-1">
                                    <li>You already have a request pending.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        @endif


        <form class="flex-1" action="{{ route('request.post') }}" method="POST" autocomplete="off">

            @csrf

            <div class="p-10">
                <div class="mb-6">
                    <label for="password" class="block text-sm font-medium text-gray-700">
                        Name
                    </label>
                    <div class="mt-1">
                        <input id="name" autocomplete="off" name="name" type="text" autocomplete="email" class="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-lime-500 focus:border-lime-500 sm:text-sm">
                    </div>
                </div>

                <div class="mb-6">
                    <label for="email" class="block text-sm font-medium text-gray-700">
                        Email address
                    </label>
                    <div class="mt-1">
                        <input id="email" value="{{ $email }}" autocomplete="off" name="email" type="email" class="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-lime-500 focus:border-lime-500 sm:text-sm">
                    </div>
                </div>

                <div class="mb-6">
                    <label for="password" class="block text-sm font-medium text-gray-700">
                        Investor number
                    </label>
                    <div class="mt-1">
                        <input placeholder="31-0123456" id="investor_number" value="{{ $investor_number }}" autocomplete="off" name="investor_number" type="text" autocomplete="email" class="investor-number appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-lime-500 focus:border-lime-500 sm:text-sm">
                    </div>
                </div>

                <div class="mb-6">
                    <label for="password" class="block text-sm font-medium text-gray-700">
                        AP number
                    </label>
                    <div class="mt-1">
                        <input placeholder="10-0123456" id="ap_number" value="{{ $ap_number }}" autocomplete="off" name="ap_number" type="text" autocomplete="email" class="apnumber appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-lime-500 focus:border-lime-500 sm:text-sm">
                    </div>
                </div>

                <div class="mb-6">
                    <label for="password" class="block text-sm font-medium text-gray-700">
                        Zip code
                    </label>
                    <div class="mt-1">
                        <input id="zip_code" value="{{ $zip_code }}" autocomplete="off" name="zip_code" type="text" autocomplete="email" class="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-lime-500 focus:border-lime-500 sm:text-sm">
                    </div>
                </div>

                <div>
                    <button type="submit" class="w-full flex justify-center py-2 px-4 border border-transparent shadow-lg font-semibold text-white bg-lime-500 hover:bg-lime-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-lime-500">
                        Continue
                    </button>
                </div>
            </div>


        </form>


    </div>

    <script>
        var cleave = new Cleave('.investor-number', {
            delimiter: '-',
            blocks: [2, 10],
            uppercase: true
        });

        var cleave = new Cleave('.apnumber', {
            delimiter: '-',
            blocks: [2, 10],
            uppercase: true
        });
    </script>
@endsection