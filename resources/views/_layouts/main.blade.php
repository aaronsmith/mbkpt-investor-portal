<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Trustee Registration Portal</title>

        <link rel="stylesheet" href="https://rsms.me/inter/inter.css">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/cleave.js@1.6.0/dist/cleave.min.js"></script>
    </head>
    <body class="antialiased bg-blue-700">

    <div class="bg-white">
        <div class="max-w-4xl mx-auto py-10 flex items-center">
            <div class="flex-1">
                <img src="{{ asset('images/logo.jpg') }}" alt="Mutual Benefits Keep Policy Trust">
            </div>
        </div>
    </div>

    <div class="bg-white">
        <div class="bg-gray-900 max-w-4xl mx-auto text-right text-white p-3" style="min-height: 30px;">

        </div>
    </div>

    <div class="bg-blue-700">
        <div class="max-w-4xl mx-auto bg-white @if(request()->routeIs('winddown')) p-10 @else p-20 @endif" @if(request()->routeIs('winddown')) style="background-image: url('images/bg-wind-down.jpg'); background-repeat: no-repeat; background-size: contain;" @endif>
            @yield('content')
        </div>
    </div>

    <div class="max-w-4xl mx-auto bg-gray-900 mt-6 mb-10 text-xs text-white p-4">
        © 2020 Mutual Benefits Keep Policy Trust
    </div>


    </body>
</html>
