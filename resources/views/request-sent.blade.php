@extends('_layouts.main')

@section('content')
    <div class="flex flex-col justify-center py-12 sm:px-6 lg:px-8">
        <div class="sm:mx-auto sm:w-full sm:max-w-xl">
            <h2 class="text-center text-3xl font-light text-gray-600">
                Request access to your account
            </h2>
        </div>

        <div class="sm:mx-auto sm:w-full sm:max-w-lg">


            <div class="flex items-center p-10">

                <div class="text-8xl text-green-600 text-center mr-3">
                    <svg class="h-20 w-20 mx-auto" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z" />
                    </svg>
                </div>

                <div class="text-sm">
                    <div class="mb-2">Your request has been submitted.</div>
                    <div class="text-gray-500">
                        We will get in touch once your information has been reviewed.
                    </div>
                </div>

            </div>



        </div>
    </div>

@endsection