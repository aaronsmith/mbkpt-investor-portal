<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ env('APP_NAME') }}</title>
    <style>
        @font-face {
            font-family: 'PT Serif';
            src: url('/fonts/pt.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;
        }

        @page {
            font-family: 'PT Serif', serif;
            font-size: 12px;
        }

        body {
            font-family: 'PT Serif', serif;
            font-size: 12px;
        }

        table{
            width: 100%;
            border-collapse: collapse;
        }

        table td{
            border-collapse: collapse;
            border: 1px solid black;
            padding: 1.5em;
        }

        table.header{
            margin-bottom: 2em;
        }

        table.header td{
            border-collapse: collapse;
            border: none;
            border-bottom: 3px double black !important;
            padding: 1em;
        }

        table td table td{
            border: none !important;
            padding: 0.15em;
            font-size: 11px;
        }

        .line-bottom{
            border-bottom: 2px solid black !important;
        }

        table.noborder td{
            border: none !important;
        }
    </style>
</head>
<body>

<table class="header" style="margin-bottom: 5rem;">
    <tr>
        <td>
            <img style="width: 350px;" src="{{ public_path('/images/logo.jpg') }}" alt="">
        </td>
    </tr>
</table>

<table class="bigger" style="margin-bottom: 10rem">
    <tr>
        <td style="vertical-align: top;">
            <div style="font-weight: bold; font-size: 1.5em;">
                VALIDATION DOCUMENT
            </div>
        </td>
        <td>
            <div>
                <div style="margin-bottom: 0.15em; font-size: 1.3rem;">
                    {{ $user->name }}
                </div>


                <div style="margin-bottom: 0.15em; font-size: 1rem;">
                    {{ $user->address }}
                </div>
                <div style="margin-bottom: 0.15em; font-size: 1rem;">
                    {{ $user->city }}, {{ $user->state }} {{ $user->zip }}
                </div>
                @if($user->country)
                <div style="margin-bottom: 0.15em; font-size: 1rem;">
                    {{ $user->country }}
                </div>
                @endif

            </div>
        </td>
    </tr>
</table>

<hr style="margin-bottom: 15rem; border-color: transparent;">

<div style="margin-bottom: 13rem;">
    <div style="font-size: 1.5em; text-align: center; margin-bottom: 0.5rem;">
        Go to {{ env('APP_URL') }}/use-code and enter the following code:<br>
    </div>
    <div style="font-size: 4em; text-align: center;">
        {{ $user->code }}
    </div>
</div>

<hr style="margin-bottom: 12rem; border-color: transparent;">


<div style="font-weight:bold;">
    <div>Mutual Benefits Keep Policy Trust</div>
    <div>c/o QCapital</div>
    <div>2424 N. Federal Hwy. Suite 460</div>
    <div>Boca Raton, FL 33431</div>
</div>
</body>
</html>
