@extends('_layouts.main')

@section('content')
    <div class="sm:mx-auto sm:w-full sm:max-w-md mb-10">
        <h2 class="text-center text-3xl font-light text-gray-500">
            Sign Up
        </h2>
        <p class="mt-2 text-center text text-gray-600 max-w">
            Or
            <a href="{{ route('login') }}" class="font-medium text-lime-600 hover:text-lime-500">
                Sign in to your account
            </a>
        </p>
    </div>

    <div class="sm:mx-auto sm:w-full sm:max-w-3xl">

        @if ($errors->any())
            <div class="rounded-md bg-red-100 p-4 w-full mx-auto mb-6">
                <div class="flex">
                    <div class="flex-shrink-0">
                        <!-- Heroicon name: x-circle -->
                        <svg class="h-5 w-5 text-red-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                            <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zM8.707 7.293a1 1 0 00-1.414 1.414L8.586 10l-1.293 1.293a1 1 0 101.414 1.414L10 11.414l1.293 1.293a1 1 0 001.414-1.414L11.414 10l1.293-1.293a1 1 0 00-1.414-1.414L10 8.586 8.707 7.293z" clip-rule="evenodd" />
                        </svg>
                    </div>
                    <div class="ml-3">
                        <h3 class="font-medium text-red-800">
                            Your submission has some errors.
                        </h3>
                        <div class="mt-2 text-sm text-red-700">
                            <ul class="list-disc pl-5 space-y-1">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        @else
            @if(session()->has('error'))
                <div class="rounded-md bg-red-100 p-4 w-full mx-auto mb-6">
                    <div class="flex">
                        <div class="flex-shrink-0">
                            <!-- Heroicon name: x-circle -->
                            <svg class="h-5 w-5 text-red-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zM8.707 7.293a1 1 0 00-1.414 1.414L8.586 10l-1.293 1.293a1 1 0 101.414 1.414L10 11.414l1.293 1.293a1 1 0 001.414-1.414L11.414 10l1.293-1.293a1 1 0 00-1.414-1.414L10 8.586 8.707 7.293z" clip-rule="evenodd" />
                            </svg>
                        </div>
                        <div class="ml-3">
                            <h3 class="font-medium text-red-800">
                                We are not able to validate your information automatically.
                            </h3>
                            <form action="{{ route('request') }}" method="get" class="mt-1">
                                @csrf

                                <button type="submit" class="flex items-center underline text-red-600 text-sm">
                                        <span class="text-red-600">
                                            Request manual validation
                                        </span>

                                    <svg class="ml-1 h-4 w-4" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M14 5l7 7m0 0l-7 7m7-7H3" />
                                    </svg>
                                </button>

                                <input type="hidden" name="email" value="{{ old('email') }}">
                                <input type="hidden" name="investor_number" value="{{ old('investor_number') }}">
                                <input type="hidden" name="last_invoice_amount" value="{{ old('last_invoice_amount') }}">
                            </form>
                        </div>
                    </div>
                </div>
            @endif

        @endif



        <div class="flex">

            <!-- This example requires Tailwind CSS v2.0+ -->
            <nav aria-label="Progress" class="mr-10 flex-1">
                <ol class="overflow-hidden">
                    <!--li class="relative pb-10">
                        <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-lime-600" aria-hidden="true"></div>
                        <a href="#" class="relative flex items-start group">
                            <span class="h-9 flex items-center">
                              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-lime-600 rounded-full group-hover:bg-lime-800">
                                <svg class="w-5 h-5 text-white" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                  <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                                </svg>
                              </span>
                            </span>
                            <span class="ml-4 min-w-0 flex flex-col">
                              <span class="text-xs font-semibold uppercase tracking-wide">Validate your identity</span>
                              <span class="text-sm text-gray-500">We need to verify your information.</span>
                            </span>
                        </a>
                    </li-->

                    <li class="relative pb-10">
                        <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-lime-600" aria-hidden="true"></div>
                        <a href="#" class="relative flex items-start group">
                                <span class="h-9 flex items-center">
                                  <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-lime-600 rounded-full">
                                    <svg class="w-5 h-5 text-white" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                      <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                                    </svg>
                                  </span>
                                </span>
                            <span class="ml-4 min-w-0 flex flex-col">
                                  <span class="text-xs font-semibold uppercase tracking-wide">Validate your identity</span>
                                  <span class="text-sm text-gray-500">We need to verify your information.</span>
                                </span>
                        </a>
                    </li>

                    <li class="relative pb-10">
                        <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
                        <a href="#" class="relative flex items-start group">
                            <span class="h-9 flex items-center" aria-hidden="true">
                              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-lime-600 rounded-full group-hover:border-gray-400">
                                    <span class="h-2.5 w-2.5 bg-lime-600 rounded-full"></span>
                              </span>
                            </span>
                            <span class="ml-4 min-w-0 flex flex-col">
                              <span class="text-xs uppercase font-semibold tracking-wide text-lime-600">Check your inbox</span>
                              <span class="text-sm text-gray-500">Be sure you have entered a valid email.</span>
                            </span>
                        </a>
                    </li>

                    <li class="relative pb-10">
                        <a href="#" class="relative flex items-start group">
                            <span class="h-9 flex items-center" aria-hidden="true">
                              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
                                <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
                              </span>
                            </span>
                            <span class="ml-4 min-w-0 flex flex-col">
                              <span class="text-xs uppercase tracking-wide text-gray-500">Create a password</span>
                              <span class="text-sm text-gray-500">Protect your account with a password.</span>
                            </span>
                        </a>
                    </li>
                </ol>
            </nav>


            <div class="flex-1">

                <div class="flex items-center">
                    <svg class="h-16 w-16 text-gray-300 mr-2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                        <path d="M2.003 5.884L10 9.882l7.997-3.998A2 2 0 0016 4H4a2 2 0 00-1.997 1.884z" />
                        <path d="M18 8.118l-8 4-8-4V14a2 2 0 002 2h12a2 2 0 002-2V8.118z" />
                    </svg>

                    <p class="text-sm">We sent an email to <span class="font-medium">{{ $user->email }}</span>, please check it.</p>
                </div>


                <p class="mt-10 border border-gray-200 p-4 rounded text-center text-sm text-gray-600">You can close this browser window and continue the signup process from there.</p>


            </div>
        </div>
    </div>
@endsection