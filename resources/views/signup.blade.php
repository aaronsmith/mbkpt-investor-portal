@extends('_layouts.main')

@section('content')
    <div class="sm:mx-auto sm:w-full sm:max-w-md mb-10">
        <h2 class="text-center text-3xl font-light text-gray-500">
            Sign Up
        </h2>
        <p class="mt-2 text-center text text-gray-600 max-w">
            Or
            <a href="{{ route('login') }}" class="font-medium text-lime-600 hover:text-lime-500">
                Sign in to your account
            </a>
        </p>

        <div class="mt-3 text-center">
            <a class="text-sm text-gray-600 hover:text-gray-500 underline" href="{{ route('signup.code') }}">If you received a code from us, click here.</a>
        </div>
    </div>

    <div class="sm:mx-auto sm:w-full sm:max-w-3xl">

        @if ($errors->any())
            <div class="rounded-md bg-red-100 p-4 w-full mx-auto mb-6">
                <div class="flex">
                    <div class="flex-shrink-0">
                        <!-- Heroicon name: x-circle -->
                        <svg class="h-5 w-5 text-red-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                            <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zM8.707 7.293a1 1 0 00-1.414 1.414L8.586 10l-1.293 1.293a1 1 0 101.414 1.414L10 11.414l1.293 1.293a1 1 0 001.414-1.414L11.414 10l1.293-1.293a1 1 0 00-1.414-1.414L10 8.586 8.707 7.293z" clip-rule="evenodd" />
                        </svg>
                    </div>
                    <div class="ml-3">
                        <h3 class="font-medium text-red-800">
                            Your submission has some errors.
                        </h3>
                        <div class="mt-2 text-sm text-red-700">
                            <ul class="list-disc pl-5 space-y-1">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        @else
            @if(session()->has('error'))
                <div class="rounded-md bg-red-100 p-4 w-full mx-auto mb-6">
                    <div class="flex">
                        <div class="flex-shrink-0">
                            <!-- Heroicon name: x-circle -->
                            <svg class="h-5 w-5 text-red-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zM8.707 7.293a1 1 0 00-1.414 1.414L8.586 10l-1.293 1.293a1 1 0 101.414 1.414L10 11.414l1.293 1.293a1 1 0 001.414-1.414L11.414 10l1.293-1.293a1 1 0 00-1.414-1.414L10 8.586 8.707 7.293z" clip-rule="evenodd" />
                            </svg>
                        </div>
                        <div class="ml-3">
                            <h3 class="font-medium text-red-800">
                                We are not able to validate your information automatically.
                            </h3>
                            <form action="{{ route('request') }}" method="get" class="mt-1">
                                @csrf

                                <button type="submit" class="flex items-center underline text-red-600 text-sm">
                                        <span class="text-red-600">
                                            Request manual validation
                                        </span>

                                    <svg class="ml-1 h-4 w-4" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M14 5l7 7m0 0l-7 7m7-7H3" />
                                    </svg>
                                </button>

                                <input type="hidden" name="email" value="{{ old('email') }}">
                                <input type="hidden" name="ap_number" value="{{ old('ap_number') }}">
                                <input type="hidden" name="investor_number" value="{{ old('investor_number') }}">
                                <input type="hidden" name="zip_code" value="{{ old('zip_code') }}">
                            </form>
                        </div>
                    </div>
                </div>
                @elseif(session()->has('error2'))
                <div class="rounded-md bg-red-100 p-4 w-full mx-auto mb-6">
                    <div class="flex">
                        <div class="flex-shrink-0">
                            <!-- Heroicon name: x-circle -->
                            <svg class="h-5 w-5 text-red-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zM8.707 7.293a1 1 0 00-1.414 1.414L8.586 10l-1.293 1.293a1 1 0 101.414 1.414L10 11.414l1.293 1.293a1 1 0 001.414-1.414L11.414 10l1.293-1.293a1 1 0 00-1.414-1.414L10 8.586 8.707 7.293z" clip-rule="evenodd" />
                            </svg>
                        </div>
                        <div class="ml-3">
                            <h3 class="font-medium text-red-800">
                                Your submission has some errors.
                            </h3>
                            <div class="mt-2 text-sm text-red-700">
                                <ul class="list-disc pl-5 space-y-1">
                                    <li>The user already exists in our database.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            @endif

        @endif



        <div class="flex">

            <!-- This example requires Tailwind CSS v2.0+ -->
            <nav aria-label="Progress" class="mr-10 flex-1">
                <ol class="overflow-hidden">
                    <!--li class="relative pb-10">
                        <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-lime-600" aria-hidden="true"></div>
                        <a href="#" class="relative flex items-start group">
                            <span class="h-9 flex items-center">
                              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-lime-600 rounded-full group-hover:bg-lime-800">
                                <svg class="w-5 h-5 text-white" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                  <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                                </svg>
                              </span>
                            </span>
                            <span class="ml-4 min-w-0 flex flex-col">
                              <span class="text-xs font-semibold uppercase tracking-wide">Validate your identity</span>
                              <span class="text-sm text-gray-500">We need to verify your information.</span>
                            </span>
                        </a>
                    </li-->

                    <li class="relative pb-10">
                        <!-- Current Step -->
                        <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
                        <a href="#" class="relative flex items-start group" aria-current="step">
                                <span class="h-9 flex items-center" aria-hidden="true">
                                  <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-lime-600 rounded-full">
                                    <span class="h-2.5 w-2.5 bg-lime-600 rounded-full"></span>
                                  </span>
                                </span>
                            <span class="ml-4 min-w-0 flex flex-col">
                                  <span class="text-xs font-semibold uppercase tracking-wide">Validate your identity</span>
                                  <span class="text-sm text-gray-500">We need to verify your information.</span>
                                </span>
                        </a>
                    </li>

                    <li class="relative pb-10">
                        <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
                        <a href="#" class="relative flex items-start group">
                            <span class="h-9 flex items-center" aria-hidden="true">
                              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
                                <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
                              </span>
                            </span>
                            <span class="ml-4 min-w-0 flex flex-col">
                              <span class="text-xs uppercase tracking-wide text-gray-500">Check your inbox</span>
                              <span class="text-sm text-gray-500">Be sure you have entered a valid email.</span>
                            </span>
                        </a>
                    </li>

                    <li class="relative pb-10">
                        <a href="#" class="relative flex items-start group">
                            <span class="h-9 flex items-center" aria-hidden="true">
                              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
                                <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
                              </span>
                            </span>
                            <span class="ml-4 min-w-0 flex flex-col">
                              <span class="text-xs uppercase tracking-wide text-gray-500">Create a password</span>
                              <span class="text-sm text-gray-500">Protect your account with a password.</span>
                            </span>
                        </a>
                    </li>
                </ol>
            </nav>


            <form class="flex-1" action="{{ route('signup.validate') }}" method="POST" autocomplete="off">

                @csrf

                <div class="mb-6">
                    <label for="email" class="block text-sm font-medium text-gray-700">
                        Email address
                    </label>
                    <div class="mt-1">
                        <input id="email" value="{{ old('email') }}" autocomplete="off" name="email" type="email" class="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-lime-500 focus:border-lime-500 sm:text-sm">
                    </div>
                </div>

                <div class="mb-6">
                    <label for="password" class="block text-sm font-medium text-gray-700">
                        Investor number
                    </label>
                    <div class="mt-1">
                        <input placeholder="31-0123456" id="investor_number" value="{{ old('investor_number') }}" autocomplete="off" name="investor_number" type="text" autocomplete="email" class="investor-number appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-lime-500 focus:border-lime-500 sm:text-sm">
                    </div>
                </div>

                <div class="mb-6">
                    <label for="password" class="block text-sm font-medium text-gray-700">
                        AP number
                    </label>
                    <div class="mt-1">
                        <input placeholder="10-0123456" id="apnumber" value="{{ old('ap_number') }}" autocomplete="off" name="ap_number" type="text" autocomplete="email" class="apnumber appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-lime-500 focus:border-lime-500 sm:text-sm">
                    </div>
                </div>

                <div class="mb-6">
                    <label for="password" class="block text-sm font-medium text-gray-700">
                        Zip code
                    </label>
                    <div class="mt-1">
                        <input id="zip_code" value="{{ old('zip_code') }}" autocomplete="off" name="zip_code" type="text" autocomplete="email" class="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-lime-500 focus:border-lime-500 sm:text-sm">
                    </div>
                </div>

                <div>
                    <button type="submit" class="w-full flex justify-center py-2 px-4 border border-transparent shadow-lg text-sm font-bold text-white bg-lime-500 hover:bg-lime-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-lime-500">
                        Continue
                    </button>
                </div>
            </form>
        </div>

        @if(!session()->has('error'))
            <div class="rounded-md bg-blue-100 p-4 w-full mx-auto mt-6">
                <div class="flex">
                    <h3 class="font-medium text-blue-800">
                        Can't locate or remember your information?
                    </h3>
                    <form action="{{ route('request') }}" method="get" class="ml-2">
                        @csrf

                        <button type="submit" class="flex items-center underline text-blue-600 text">
                                    <span class="text-blue-600">
                                        Request manual validation
                                    </span>

                            <svg class="ml-1 h-4 w-4" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M14 5l7 7m0 0l-7 7m7-7H3" />
                            </svg>
                        </button>

                        <input type="hidden" name="email" value="{{ old('email') }}">
                        <input type="hidden" name="investor_number" value="{{ old('investor_number') }}">
                        <input type="hidden" name="ap_number" value="{{ old('ap_number') }}">
                        <input type="hidden" name="zip_code" value="{{ old('zip_code') }}">
                    </form>
                </div>
            </div>
        @endif
    </div>

    <script>
        var cleave = new Cleave('.investor-number', {
            delimiter: '-',
            blocks: [2, 10],
            uppercase: true
        });

        var cleave = new Cleave('.apnumber', {
            delimiter: '-',
            blocks: [2, 10],
            uppercase: true
        });
    </script>
@endsection