@extends('_layouts.main')

@section('content')
    <!-- This example requires Tailwind CSS v2.0+ -->

    <div class="sm:mx-auto sm:w-full mb-10">
        <h2 class="text-center text-3xl font-light text-gray-500">
            Activate your account with a code
        </h2>
        <p class="mt-2 text-center text text-gray-600 max-w">
            Or
            <a href="{{ route('signup') }}" class="font-medium text-lime-600 hover:text-lime-500">
                Sign up
            </a>
        </p>
    </div>


    <div class="mt-8 sm:mx-auto sm:w-full sm:max-w-2xl">

        @if ($errors->any())
            <div class="rounded-md bg-red-100 p-4 w-full mx-auto mb-6">
                <div class="flex">
                    <div class="flex-shrink-0">
                        <!-- Heroicon name: x-circle -->
                        <svg class="h-5 w-5 text-red-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                            <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zM8.707 7.293a1 1 0 00-1.414 1.414L8.586 10l-1.293 1.293a1 1 0 101.414 1.414L10 11.414l1.293 1.293a1 1 0 001.414-1.414L11.414 10l1.293-1.293a1 1 0 00-1.414-1.414L10 8.586 8.707 7.293z" clip-rule="evenodd" />
                        </svg>
                    </div>
                    <div class="ml-3">
                        <h3 class="font-medium text-red-800">
                            Your submission has some errors.
                        </h3>
                        <div class="mt-2 text-sm text-red-700">
                            <ul class="list-disc pl-5 space-y-1">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        @elseif(session()->has('error'))
            <div class="rounded-md bg-red-100 p-4 w-full mx-auto mb-6">
                <div class="flex">
                    <div class="flex-shrink-0">
                        <!-- Heroicon name: x-circle -->
                        <svg class="h-5 w-5 text-red-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                            <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zM8.707 7.293a1 1 0 00-1.414 1.414L8.586 10l-1.293 1.293a1 1 0 101.414 1.414L10 11.414l1.293 1.293a1 1 0 001.414-1.414L11.414 10l1.293-1.293a1 1 0 00-1.414-1.414L10 8.586 8.707 7.293z" clip-rule="evenodd" />
                        </svg>
                    </div>
                    <div class="ml-3">
                        <h3 class="font-medium text-red-800">
                            Your submission has some errors.
                        </h3>
                        <div class="mt-2 text-sm text-red-700">
                            <ul class="list-disc pl-5 space-y-1">
                                <li>Entered information is invalid.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        @endif



        <div class="flex max-w-md mx-auto bg-gray-50 py-8 px-4 shadow sm:rounded-lg sm:px-10">

            <form class="flex-1" action="{{ route('signup.code.post') }}" method="POST" autocomplete="off">
                @csrf
                <div class="mb-6">
                    <label for="email" class="block text-sm font-medium text-gray-700 mb-3">
                        Email
                    </label>
                    <div class="mt-1">
                        <input id="email" autocomplete="off" name="email" type="email" autocomplete="email" class="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-lime-500 focus:border-lime-500 sm:text-sm">
                    </div>
                </div>

                <div class="mb-6">
                    <label for="email" class="block text-sm font-medium text-gray-700 mb-3">
                        Enter the code you received from us
                    </label>
                    <div class="mt-1">
                        <input id="email" autocomplete="off" name="code" type="code" autocomplete="email" class="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-lime-500 focus:border-lime-500 sm:text-sm">
                    </div>
                </div>

                <div>
                    <button type="submit" class="w-full flex justify-center py-2 px-4 border border-transparent shadow-lg text-sm font-semibold text-white bg-lime-500 hover:bg-lime-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-lime-500">
                        Continue
                    </button>
                </div>
            </form>
        </div>
    </div>

    <script>
        function viewTerms(){
            document.getElementById("terms").classList.remove('hidden')
        }

        function hideTerms(){
            document.getElementById("terms").classList.add('hidden')
        }
    </script>
@endsection