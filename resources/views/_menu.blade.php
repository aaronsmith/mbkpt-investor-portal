<nav class="flex-1 flex flex-col overflow-y-auto" aria-label="Sidebar">
    <div class="px-2 space-y-1">
        <a href="{{ route('faq-auth') }}" class="@if(request()->routeIs('faq*')) bg-lime-800 @endif text-white group flex items-center px-2 py-2 text-sm leading-6 font-medium rounded-md text-lime-100 hover:text-white hover:bg-lime-600">
            <svg xmlns="http://www.w3.org/2000/svg" class="mr-4 h-6 w-6 text-lime-200" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9.663 17h4.673M12 3v1m6.364 1.636l-.707.707M21 12h-1M4 12H3m3.343-5.657l-.707-.707m2.828 9.9a5 5 0 117.072 0l-.548.547A3.374 3.374 0 0014 18.469V19a2 2 0 11-4 0v-.531c0-.895-.356-1.754-.988-2.386l-.548-.547z" />
            </svg>
            FAQ
        </a>
    </div>
    <div class="px-2 space-y-1">
        <a href="{{ route('questions') }}" class="@if(request()->routeIs('questions*')) bg-lime-800 @endif text-white group flex items-center px-2 py-2 text-sm leading-6 font-medium rounded-md text-lime-100 hover:text-white hover:bg-lime-600">
            <!-- Heroicon name: document-report -->
            <svg xmlns="http://www.w3.org/2000/svg" class="mr-4 h-6 w-6 text-lime-200" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17 8h2a2 2 0 012 2v6a2 2 0 01-2 2h-2v4l-4-4H9a1.994 1.994 0 01-1.414-.586m0 0L11 14h4a2 2 0 002-2V6a2 2 0 00-2-2H5a2 2 0 00-2 2v6a2 2 0 002 2h2v4l.586-.586z" />
            </svg>
            My Questions
        </a>
    </div>
    <div class="mt-6 pt-6 border-t border-lime-800">
        <div class="px-2 space-y-1">
            <a href="{{ route('profile') }}" class="@if(request()->routeIs('profile')) bg-lime-800 @endif group flex items-center px-2 py-2 text-sm leading-6 font-medium rounded-md text-lime-100 hover:text-white hover:bg-lime-600">
                <!-- Heroicon name: cog -->
                <svg class="mr-4 h-6 w-6 text-lime-200" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10.325 4.317c.426-1.756 2.924-1.756 3.35 0a1.724 1.724 0 002.573 1.066c1.543-.94 3.31.826 2.37 2.37a1.724 1.724 0 001.065 2.572c1.756.426 1.756 2.924 0 3.35a1.724 1.724 0 00-1.066 2.573c.94 1.543-.826 3.31-2.37 2.37a1.724 1.724 0 00-2.572 1.065c-.426 1.756-2.924 1.756-3.35 0a1.724 1.724 0 00-2.573-1.066c-1.543.94-3.31-.826-2.37-2.37a1.724 1.724 0 00-1.065-2.572c-1.756-.426-1.756-2.924 0-3.35a1.724 1.724 0 001.066-2.573c-.94-1.543.826-3.31 2.37-2.37.996.608 2.296.07 2.572-1.065z" />
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z" />
                </svg>
                Profile
            </a>

            <a href="{{ route('logout') }}" class="group flex items-center px-2 py-2 text-sm leading-6 font-medium rounded-md text-lime-100 hover:text-white hover:bg-lime-600">
                <!-- Heroicon name: shield-check -->
                <svg class="mr-4 h-6 w-6 text-lime-200" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m5.618-4.016A11.955 11.955 0 0112 2.944a11.955 11.955 0 01-8.618 3.04A12.02 12.02 0 003 9c0 5.591 3.824 10.29 9 11.622 5.176-1.332 9-6.03 9-11.622 0-1.042-.133-2.052-.382-3.016z" />
                </svg>
                Logout
            </a>
        </div>
    </div>
</nav>
