<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', function () {
    return view('welcome');
})->name('login');

Route::get('/', function () {
    $user = \App\Models\User::find(1);
    ray()->model($user);
    return view('winddown');
})->name('winddown');

Route::post('/', '\App\Http\Controllers\UserController@login')->name('login.post');
Route::post('/password-update', '\App\Http\Controllers\UserController@passwordUpdate')->name('password.update');
Route::get('/logout', '\App\Http\Controllers\UserController@logout')->name('logout');

Route::get('/signup', '\App\Http\Controllers\UserController@signup')->name('signup');
Route::post('/signup', '\App\Http\Controllers\UserController@validateUser')->name('signup.validate');
Route::get('/signup/password', '\App\Http\Controllers\UserController@password')->name('signup.password');
Route::get('/signup/{user}/password', '\App\Http\Controllers\UserController@passwordWithUser')->name('signup.password-with-user');
Route::post('/signup/password', '\App\Http\Controllers\UserController@passwordSave')->name('signup.password.save');

Route::get('/faq', '\App\Http\Controllers\UserController@faq')->name('faq');
Route::get('/request', '\App\Http\Controllers\UserController@request')->name('request');
Route::post('/request-post', '\App\Http\Controllers\UserController@requestPost')->name('request.post');
Route::get('/request-sent', '\App\Http\Controllers\UserController@requestSent')->name('request.sent');
Route::get('/validate-email/check/{token}', '\App\Http\Controllers\UserController@validateEmailTemp')->name('signup.validate-email-temp');
Route::get('/validate-email/{token}', '\App\Http\Controllers\UserController@validateEmail')->name('signup.validate-email');
Route::get('/signup-error', '\App\Http\Controllers\UserController@signupError')->name('signup.error');
Route::get('/use-code', '\App\Http\Controllers\UserController@signupWithCode')->name('signup.code');
Route::post('/use-code', '\App\Http\Controllers\UserController@signupWithCodePost')->name('signup.code.post');

Route::middleware(['auth'])->group(function(){
    Route::get('/dashboard', '\App\Http\Controllers\UserController@dashboard')->name('dashboard');
    Route::get('/download/{file}', '\App\Http\Controllers\UserController@download')->name('download');
    Route::get('/profile', '\App\Http\Controllers\UserController@profile')->name('profile');
    Route::get('/questions', '\App\Http\Controllers\QuestionController@index')->name('questions');
    Route::get('/questions/ask', '\App\Http\Controllers\QuestionController@ask')->name('questions.ask');
    Route::post('/questions', '\App\Http\Controllers\QuestionController@save')->name('questions.save');
    Route::get('/questions/{question}', '\App\Http\Controllers\QuestionController@view')->name('questions.view')->middleware(['auth']);
    Route::get('/faq-auth', '\App\Http\Controllers\UserController@faqAuth')->name('faq-auth');
});

Route::get('/admin/login', function () {
    return view('admin.login');
})->name('admin.login');

Route::post('/admin/login', '\App\Http\Controllers\AdminController@login')->name('admin.login.post');
Route::get('/admin/logout', '\App\Http\Controllers\AdminController@logout')->name('admin.logout');

Route::prefix('admin')->middleware('auth.admin')->group(function () {
    Route::get('/', '\App\Http\Controllers\AdminController@dashboard')->name('admin.dashboard');
    Route::post('/upload', '\App\Http\Controllers\FileController@upload')->name('admin.upload');
    Route::get('/delete/{file}', '\App\Http\Controllers\FileController@delete')->name('admin.delete');
    Route::get('/requests', '\App\Http\Controllers\RequestController@index')->name('admin.requests');
    Route::get('/requests/{user}/approve', '\App\Http\Controllers\RequestController@approve')->name('admin.requests.approve');
    Route::get('/requests/{user}/generate', '\App\Http\Controllers\RequestController@generate')->name('admin.requests.generate');
    Route::post('/requests/generate', '\App\Http\Controllers\RequestController@generateMass')->name('admin.requests.generate-mass');
    Route::get('/users', '\App\Http\Controllers\UserController@adminIndex')->name('admin.users');
    Route::get('/users/{user}', '\App\Http\Controllers\UserController@adminEdit')->name('admin.users.edit');
    Route::post('/users/{user}', '\App\Http\Controllers\UserController@adminUpdate')->name('admin.users.update');
    Route::get('/users/{user}/delete', '\App\Http\Controllers\UserController@adminDelete')->name('admin.users.delete');
    Route::get('/users/{user}/history', '\App\Http\Controllers\UserController@history')->name('admin.users.history');
    Route::get('/roles', '\App\Http\Controllers\PermissionController@index')->name('admin.roles');
    Route::post('/roles', '\App\Http\Controllers\PermissionController@save')->name('admin.roles.save');
    Route::post('/roles/{role}', '\App\Http\Controllers\PermissionController@update')->name('admin.roles.update');
    Route::get('/faq', '\App\Http\Controllers\FaqController@index')->name('admin.faq');
    Route::get('/faq/create', '\App\Http\Controllers\FaqController@create')->name('admin.faq.create');
    Route::post('/faq', '\App\Http\Controllers\FaqController@save')->name('admin.faq.save');
    Route::get('/faq/{faq}', '\App\Http\Controllers\FaqController@edit')->name('admin.faq.edit');
    Route::post('/faq/{faq}', '\App\Http\Controllers\FaqController@update')->name('admin.faq.update');
    Route::get('/faq/{faq}/delete', '\App\Http\Controllers\FaqController@delete')->name('admin.faq.delete');
    Route::get('/questions', '\App\Http\Controllers\QuestionController@adminIndex')->name('admin.questions');
    Route::get('/questions/history', '\App\Http\Controllers\QuestionController@history')->name('admin.questions.history');
    Route::get('/questions/{question}/view', '\App\Http\Controllers\QuestionController@adminView')->name('admin.questions.view');
    Route::get('/questions/{question}', '\App\Http\Controllers\QuestionController@answer')->name('admin.questions.answer');
    Route::post('/questions/{question}', '\App\Http\Controllers\QuestionController@saveAnswer')->name('admin.questions.answer.save');
});
