<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Te7aHoudini\LaravelTrix\Traits\HasTrixRichText;

class Faq extends Model
{
    protected $table = "faq";
    protected $guarded = [];
    use HasTrixRichText;
}
