<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Te7aHoudini\LaravelTrix\Traits\HasTrixRichText;

class UserQuestion extends Model
{
    protected $table = "user_questions";
    protected $guarded = [];
    protected $dates = ['answer_date', 'created_at', 'updated_at', 'deleted_at'];

    use HasTrixRichText;

    function user(){
        return $this->belongsTo('\App\Models\User');
    }

    function admin(){
        return $this->belongsTo('\App\Models\Admin', 'answered_by');
    }
}
