<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;

class Admin extends Authenticatable {
    use HasFactory, Notifiable;

    function getFirstNameAttribute(): string
    {
        return explode(" ", $this->name)[0];
    }

    public function getInitialsAttribute(): string
    {
        $name_array = explode(' ',trim($this->name));

        $firstWord = $name_array[0];
        $lastWord = $name_array[count($name_array)-1];

        return Str::upper($firstWord[0]."".$lastWord[0]);
    }
}