<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use Laratrust\Traits\LaratrustUserTrait;

class User extends Authenticatable
{
    use LaratrustUserTrait;
    use HasFactory, Notifiable;
    use SoftDeletes;

    protected $dates = ['request_date'];

    protected $fillable = [
        'name',
        'email',
        'password'
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    function getFirstNameAttribute(): string
    {
        return explode(" ", $this->name)[0];
    }

    public function getInitialsAttribute(): string
    {
        $name_array = explode(' ',trim($this->name));

        $firstWord = $name_array[0];
        $lastWord = $name_array[count($name_array)-1];

        return Str::upper($firstWord[0]."".$lastWord[0]);
    }

    function logs(){
        return $this->hasMany('\App\Models\Log');
    }
}
