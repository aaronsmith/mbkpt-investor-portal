<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    use HasFactory;
    protected $guarded = [];

    function item(){
        return $this->belongsTo('\App\Models\LogItem', 'log_item_id');
    }
}
