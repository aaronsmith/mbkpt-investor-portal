<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestValidation extends FormRequest
{
    public function rules(): array
    {
        return [
            'name' => 'required',
            'email' => 'required|email',
            'investor_number' => 'required',
            'ap_number' => 'required',
            'zip_code' => 'required',
        ];
    }
}
