<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FileValidation extends FormRequest
{
    public function rules(): array
    {
        return [
            'name' => 'required',
            'file' => 'required|mimes:pdf,xls,xlsx,doc,docx,ppt,pptx'
        ];
    }
}
