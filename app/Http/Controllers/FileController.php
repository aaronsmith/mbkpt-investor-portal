<?php

namespace App\Http\Controllers;

use App\Http\Requests\FileValidation;
use App\Models\File;
use App\Models\Log;
use Illuminate\Http\Request;
use Storage;

class FileController extends Controller
{
    function upload(FileValidation $request){
        $path = $request->file('file')->store('public/files');
        $ext = pathinfo(storage_path($path), PATHINFO_EXTENSION);

        //the file into.
        $pdfVersion = "1.4";

        //The path that you want to save the new
        //file to
        $newFileName = "public/files/" . uniqid("FILE", true) . ".pdf";
        $newFile = Storage::path($newFileName);

        //The path of the file that you want
        //to convert
        $currentFile = $path;

        //Create the GhostScript command
        $gsCmd = "gs -sDEVICE=pdfwrite -dPDFSETTINGS=/printer -dCompatibilityLevel=$pdfVersion -dNOPAUSE -dBATCH -dPDFSETTINGS=/ebook -sOutputFile=$newFile $currentFile";

        //Run it using PHP's exec function.
        exec($gsCmd);

        $file = new File();
        $file->name = $request->get('name');
        $file->path = $newFileName;
        $file->extension = $ext;
        $file->save();

        return redirect()->route('admin.dashboard');
    }

    function delete(File $file){
        $file->delete();
        return redirect()->route('admin.dashboard');
    }


}
