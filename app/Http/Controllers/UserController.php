<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginValidation;
use App\Http\Requests\PasswordValidation;
use App\Http\Requests\RequestValidation;
use App\Http\Requests\UserValidation;
use App\Mail\QuestionAnswered;
use App\Mail\UserRequest;
use App\Mail\ValidateEmail;
use App\Models\Admin;
use App\Models\Faq;
use App\Models\File;
use App\Models\Log;
use App\Models\Role;
use App\Models\User;
use Carbon\Carbon;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Mail;
use QrCode;
use Storage;
use setasign\Fpdi\Fpdi;


class UserController extends Controller
{
    static function crypto_rand_secure($min, $max)
    {
        $range = $max - $min;
        if ($range < 1) return $min; // not so random...
        $log = ceil(log($range, 2));
        $bytes = (int) ($log / 8) + 1; // length in bytes
        $bits = (int) $log + 1; // length in bits
        $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
        do {
            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
            $rnd = $rnd & $filter; // discard irrelevant bits
        } while ($rnd > $range);
        return $min + $rnd;
    }

    static function getToken($length)
    {
        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet.= "0123456789";
        $max = strlen($codeAlphabet); // edited

        for ($i=0; $i < $length; $i++) {
            $token .= $codeAlphabet[QuestionController::crypto_rand_secure(0, $max-1)];
        }

        return $token;
    }

    function signup(){
        return view('signup');
    }

    function signupWithCode(){
        return view('signupWithCode');
    }

    function signupWithCodePost(Request $request){
        $request->validate([
            'email' => 'email|required',
            'code' => 'required'
        ]);

        $isValid = false;

        if($user = User::whereNull('password')->where('email', $request->get('email'))->where('code', $request->get('code'))->first()){
            $isValid = true;
        }

        if($isValid){
            Log::create(['log_item_id' => 13, 'user_id' => $user->id, 'ip'=> request()->ip()]);
            Log::create(['log_item_id' => 4, 'user_id' => $user->id, 'ip'=> request()->ip()]);
            Mail::to($user->email)->send(new ValidateEmail($user));
            return redirect()->route('signup.validate-email-temp', $user->token);
        }

        session()->flash('error', 'validation-failed');
        return redirect()->back();
    }

    function login(LoginValidation $request){
        $credentials = $request->only(['email', 'password']);
        if($user = \Auth::attempt($credentials, $request->get('remember_me'))){
            Log::create(['log_item_id' => 1, 'user_id' => \Auth::user()->id, 'ip'=> request()->ip()]);
            //return redirect()->route('dashboard');
            return redirect()->intended('questions');
        }

        if($user = User::where('email', $request->get('email'))->first()){
            Log::create(['log_item_id' => 3, 'user_id' => $user->id, 'ip'=> request()->ip()]);
        }

        session()->flash('error', 'login-error');
        return redirect()->route('login')->withInput();
    }

    function validateUser(UserValidation $request): \Illuminate\Http\RedirectResponse
    {
        $isValid = false;

        if(User::where('password', '<>', null)->where(function($query) use ($request){
            $query->orWhere('email', $request->get('email'));
            $query->orWhere('investor_number', $request->get('investor_number'));
        })->exists()){
            session()->flash('error2', 'user-exists');
            return redirect()->route('signup')->withInput();
        }

        if($user = User::whereNull('password')->where('zip', $request->get('zip_code'))->where('email', $request->get('email'))->where('investor_number', $request->get('investor_number'))->where('apnumber', $request->get('ap_number'))->first()){
            /*$lastInvoiceAmount = round($user->last_invoice_amount);
            $lastInvoiceAmountUser = round($request->get('last_invoice_amount'));

            if($lastInvoiceAmount == $lastInvoiceAmountUser){
            }*/


            $isValid = true;
        }

        if($isValid){
            // Send Email
            Log::create(['log_item_id' => 4, 'user_id' => $user->id, 'ip'=> request()->ip()]);
            Mail::to($user->email)->send(new ValidateEmail($user));
            return redirect()->route('signup.validate-email-temp', $user->token);
        }

        if($user = User::where('email', $request->get('email'))->first()){
            Log::create(['log_item_id' => 5, 'user_id' => $user->id, 'ip'=> request()->ip()]);
        }

        session()->flash('error', 'validation-failed');

        return redirect()->route('signup')->withInput();
    }

    function validateEmailTemp($token)
    {
        if($user = User::where('token', $token)->first()){
            return view('signup-check-email', [
                'user' => $user
            ]);
        }

        return redirect()->route('signup.error');

    }

    function validateEmail($token): \Illuminate\Http\RedirectResponse
    {
        if($user = User::where('token', $token)->first()){
            Log::create(['log_item_id' => 6, 'user_id' => $user->id, 'ip'=> request()->ip()]);
            Log::create(['log_item_id' => 1, 'user_id' => $user->id, 'ip'=> request()->ip()]);

            \Auth::login($user);
            \Auth::user()->email_verified_at = Carbon::now();
            \Auth::user()->save();
            return redirect()->route('signup.password');
        }

        return redirect()->route('signup.error');
    }

    function signupError(){
        return view('error');
    }

    function password(){
        return view('signup2');
    }

    function passwordWithUser(User $user){
        \Auth::login($user);
        return view('signup2');
    }

    function passwordSave(PasswordValidation $request): \Illuminate\Http\RedirectResponse
    {
        Log::create(['log_item_id' => 7, 'user_id' => \Auth::user()->id, 'ip'=> request()->ip()]);

        \Auth::user()->password = \Hash::make($request->get('password'));
        \Auth::user()->approval_date = Carbon::now();
        \Auth::user()->save();


        return redirect()->route('dashboard');
    }

    function dashboard(){
        $files = File::orderBy('name')->get();
        return view('dashboard', [
            'files' => $files
        ]);
    }

    function download(File $file){
        // Generate QR Code
        //QrCode::generate(\Auth::user()->investor_number . "", Storage::path("public/files/" . \Auth::user()->investor_number.".png"));
        $image = \QrCode::format('png')
            ->size(80)->errorCorrection('H')
            ->generate(\Auth::user()->name . " - Div INumber: " . \Auth::user()->investor_number . " - Date: " . Carbon::now()->format("m-d-Y"));
        $output_file = "public/files/" . \Auth::user()->investor_number.".png";
        Storage::disk('local')->put($output_file, $image); //storage/app/public/img/qr-code/img-1557309130.png

        // Open pdf and put qr
        $pdf = new Fpdi();

        // get the page count
        $pageCount = $pdf->setSourceFile(Storage::path($file->path));

        // iterate through all pages
        for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++)
        {
            // import a page
            $templateId = $pdf->importPage($pageNo);
            // get the size of the imported page
            $size = $pdf->getTemplateSize($templateId);

            $pdf->AddPage('L');

            // use the imported page
            $pdf->useTemplate($templateId);

            $imagePath = Storage::path("public/files/" . \Auth::user()->investor_number.".png");
            $pdf->Image($imagePath,5,180);
        }


        $pdf->Output(Storage::path("public/files/" . \Auth::user()->investor_number . ".pdf"), 'F', true);

        Log::create(['log_item_id' => 12, 'user_id' => \Auth::user()->id, 'ip'=> request()->ip()]);
        return Storage::download("public/files/" . \Auth::user()->investor_number . ".pdf");
    }

    function logout(){
        if(\Auth::check()){
            Log::create(['log_item_id' => 9, 'user_id' => \Auth::user()->id, 'ip'=> request()->ip()]);

            \Auth::logout();
        }

        return redirect()->route('login');
    }

    function profile(){
        return view('profile');
    }

    function passwordUpdate(PasswordValidation $request): \Illuminate\Http\RedirectResponse
    {
        Log::create(['log_item_id' => 8, 'user_id' => \Auth::user()->id, 'ip'=> request()->ip()]);

        \Auth::user()->password = Hash::make($request->get('password'));
        session()->flash('success', 'password-updated');
        return redirect()->route('profile');
    }

    function request(Request $request){
        $email = $request->get('email');
        $investor_number = $request->get('investor_number');
        $ap_number = $request->get('ap_number');
        $zip_code = $request->get('zip_code');

        return view('request', [
            'email' => $email,
            'investor_number' => $investor_number,
            'ap_number' => $ap_number,
            'zip_code' => $zip_code
        ]);
    }

    function requestPost(RequestValidation $request){
        if(User::where('is_request', true)->whereNull('approval_date')->where('request_ip', request()->ip())->exists()){
            session()->flash('request-ip-error', 'request-ip-error');
            return redirect()->back();
        }

        if($user = User::whereNull('password')->where('email', $request->get('email'))->first()){
            $user->name = $request->get('name');
            $user->is_request = true;
            $user->request_date = Carbon::now();
            $user->request_ip = $request->ip();
            $user->token = uniqid();
            $user->code = QuestionController::getToken(6);
        }elseif($user = User::whereNull('password')->where('investor_number', $request->get('investor_number'))->first()){
            $user->name = $request->get('name');
            $user->is_request = true;
            $user->request_date = Carbon::now();
            $user->request_ip = $request->ip();
            $user->token = uniqid();
            $user->code = QuestionController::getToken(6);
        }else{
            $user = new User();
            $user->name = $request->get('name');
            $user->email = $request->get('email');

            if($request->has('investor_number') && !empty($request->get('investor_number'))){
                $user->investor_number = $request->get('investor_number');

                $user->div = explode("-", $request->get('investor_number'))[0];
                $user->inumber = explode("-", $request->get('investor_number'))[1];
            }

            if($request->has('ap_number') && !empty($request->get('ap_number'))) {
                $user->apnumber = $request->get('ap_number');
            }

            if($request->has('zip_code') && !empty($request->get('zip_code'))) {
                $user->zip = $request->get('zip_code');
            }

            $user->is_request = true;
            $user->request_date = Carbon::now();
            $user->request_ip = $request->ip();
            $user->token = uniqid();
            $user->code = QuestionController::getToken(6);
        }

        $user->syncRoles([1,2]);

        $user->save();

        foreach(Admin::all() as $admin){
            Mail::to($admin->email)->send(new UserRequest($user));
        }

        Log::create(['log_item_id' => 10, 'user_id' => $user->id, 'ip'=> request()->ip()]);

        return redirect()->route('request.sent');
    }

    function requestSent(){
        return view('request-sent');
    }

    function adminIndex(){
        //Filters
        $is_registered = '';
        $has_downloaded = '';
        $search_st = '';
        $totalRegistered = User::whereNotNull('password')->count();

        $users = User::orderBy('name');



        if(request()->has('is_registered') && !empty(request()->get('is_registered'))){
            $is_registered = request()->get('is_registered');

            if($is_registered == 'yes'){
                $users = $users->whereNotNull('password');
            }elseif ($is_registered == 'failed') {
                $users = $users->whereNull('password')->where('is_request', false)->whereHas('logs', function ($query) {
                        $query->where('log_item_id', 5);
                    });
            }else{
                $users = $users->whereNull('password')->where('is_request', false);
            }
        }else{
            $users = $users->where('is_request', false);
        }

        if(request()->has('has_downloaded') && !empty(request()->get('has_downloaded'))){
            $has_downloaded = request()->get('has_downloaded');
            $users = $users->whereHas('logs', function ($query) {
                $query->where('log_item_id', 12);
            });
        }

        if(request()->has('search_st') && !empty(request()->get('search_st'))){
            $search_st = request()->get('search_st');
            $users = $users->where(function($query) use ($search_st){
                $query->orWhere('name', 'like', '%' . $search_st . '%');
                $query->orWhere('email', 'like', '%' . $search_st . '%');
                $query->orWhere('div', 'like', '%' . $search_st . '%');
                $query->orWhere('inumber', 'like', '%' . $search_st . '%');
                $query->orWhere('investor_number', 'like', '%' . $search_st . '%');
                $query->orWhere('apnumber', 'like', '%' . $search_st . '%');
            });
        }

        $users = $users->paginate(20)->withQueryString();


        return view('admin.users', [
            'users' => $users,
            'has_downloaded' => $has_downloaded,
            'is_registered' => $is_registered,
            'search_st' => $search_st,
            'totalRegistered' => $totalRegistered,
        ]);
    }

    function adminEdit(User $user){
        $roles = Role::orderBy('name')->get();

        return view('admin.users-edit', [
            'user' => $user,
            'roles' => $roles
        ]);
    }

    function history(User $user){
        return view('admin.user-history', [
            'user' => $user
        ]);
    }

    function adminUpdate(User $user, Request $request){
        $request->validate([
            'name' => 'required',
            'email' => 'email|required',
            'div' => 'required',
            'inumber' => 'required',
            'ap_number' => 'required'
        ]);

        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->address = $request->get('address');
        $user->city = $request->get('city');
        $user->country = $request->get('country');
        $user->state = $request->get('state');
        $user->div = $request->get('div');
        $user->zip = $request->get('zip');
        $user->inumber = $request->get('inumber');
        $user->apnumber = $request->get('ap_number');
        $user->investor_number = $request->get('div') . "-" . $request->get('inumber');
        $user->save();

        if(is_array($request->get('roles')) && count($request->get('roles')) > 0){
            $user->syncRoles($request->get('roles'));
        }else{
            $user->syncRoles([]);
        }

        session()->flash('success', $user->name);

        if($request->has('return')){
            return redirect()->route('admin.requests');
        }

        return redirect()->route('admin.users');
    }

    function adminDelete(User $user, Request $request){
        $user->delete();

        return redirect()->back();
    }

    function faq(){
        $questions = Faq::orderBy('position')->get();
        return view('faq',[
            'questions' => $questions
        ]);
    }

    function faqAuth(){
        $questions = Faq::orderBy('position')->get();
        return view('faq-auth',[
            'questions' => $questions
        ]);
    }
}
