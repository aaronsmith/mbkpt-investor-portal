<?php

namespace App\Http\Controllers;

use App\Mail\QuestionAnswered;
use App\Models\Log;
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Mail;
use Storage;

class PermissionController extends Controller
{
    function index(){
        $roles = Role::orderBy('name')->get();
        $permissions = Permission::orderBy('name')->get();

        $currentRole = null;
        if(\request()->has('current_role') && !empty(request()->get('current_role'))){
            $currentRole = Role::find(request()->get('current_role'));
        }


        return view('admin.roles.index', [
            'roles' => $roles,
            'permissions' => $permissions,
            'currentRole' => $currentRole
        ]);
    }

    function save(Request $request){
        $request->validate([
            'name' => 'required'
        ]);

        $role = new Role();
        $role->name = $request->get('name');
        $role->save();

        return redirect()->route('admin.roles');
    }

    function update(Request $request, Role $role){
        $role->syncPermissions($request->get('permissions'));

        session()->flash('success', 'role-updated');

        return redirect()->route('admin.roles', [
            'current_role' => $role->id
        ]);
    }
}
