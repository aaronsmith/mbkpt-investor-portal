<?php

namespace App\Http\Controllers;

use App\Mail\QuestionAnswered;
use App\Models\Log;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Mail;
use Storage;

class RequestController extends Controller
{
    function index(){
        $requests = User::where('is_request', true)->whereNull('password')->orderBy('id')->get();
        return view('admin.requests', [
            'requests' => $requests
        ]);
    }

    function approve(User $user){

        /*Log::create(['log_item_id' => 11, 'user_id' => $user->id]);

        $user->approval_date = Carbon::now();
        $user->approval_type = "manual";
        $user->approved_by = \Auth::guard('admin')->user()->id;
        $user->save();

        Mail::to($user->email)->send(new AccountApproved($user));

        return redirect()->back()*/
    }

    static function generate(User $user){

        $data = [
            'user' => $user
        ];

        $pdf = \PDF::loadView('_pdf.code', $data)->output();

        \Storage::put("public/files/{$user->investor_number}_code.pdf", $pdf);
        Log::create(['log_item_id' => 15, 'user_id' => $user->id]);

        /*Log::create(['log_item_id' => 11, 'user_id' => $user->id]);

        $user->approval_date = Carbon::now();
        $user->approval_type = "manual";
        $user->approved_by = \Auth::guard('admin')->user()->id;
        $user->save();

        Mail::to($user->email)->send(new AccountApproved($user));
        */
        //return Storage::download("public/files/{$user->investor_number}_code.pdf");

        //return redirect()->back();
    }

    function generateMass(Request $request){
        $requests = User::whereIn('id', $request->get('requests'))->get();
        foreach($requests as $r){
            RequestController::generate($r);
        }

        $pdf = new \Clegginabox\PDFMerger\PDFMerger();

        foreach($requests as $r) {
            $pdf->addPDF(storage_path("app/public/files/" . $r->investor_number . "_code.pdf"), 'all');
        }

        $uid = uniqid("Validation");
        $pdf->merge('file', storage_path("app/public/files/" . $uid . ".pdf"), 'P');
        return Storage::download("public/files/{$uid}.pdf");
    }
}
