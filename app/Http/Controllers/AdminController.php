<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginValidation;
use App\Models\File;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    function login(LoginValidation $request): \Illuminate\Http\RedirectResponse
    {
        $credentials = $request->only(['email', 'password']);
        if($user = \Auth::guard('admin')->attempt($credentials, $request->get('remember_me'))){
            return redirect()->route('admin.dashboard');
        }

        session()->flash('error', 'login-error');
        return redirect()->route('admin.login')->withInput();
    }

    function dashboard(){
        $files = File::orderBy('name')->get();


        return view('admin.dashboard', [
            'files' => $files
        ]);
    }

    function logout(){
        \Auth::guard('admin')->logout();
        return redirect()->route('admin.login');
    }

}
