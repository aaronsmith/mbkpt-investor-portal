<?php

namespace App\Http\Controllers;

use App\Mail\QuestionAnswered;
use App\Models\Faq;
use App\Models\Log;
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Mail;
use Storage;

class FaqController extends Controller
{
    function index(){
        $questions = Faq::orderBy('position')->get();


        return view('admin.faq.index', [
            'questions' => $questions
        ]);
    }

    function create(){
        return view('admin.faq.create');
    }

    function save(Request $request){
        $request->validate([
            'question' => 'required',
            'position' => 'integer'
        ]);

        $position = 0;
        if($request->has('position') && !empty($request->get('position'))){
            $position = $request->get('position');
        }

        Faq::create([
            'question' => $request->get('question'),
            'answer' => '',
            'position' => $position,
            'faq-trixFields' => request('faq-trixFields'),
        ]);

        return redirect()->route('admin.faq');
    }

    function edit(Faq $faq){
        return view('admin.faq.edit', [
            'faq' => $faq
        ]);
    }

    function update(Request $request, Faq $faq){
        $request->validate([
            'question' => 'required',
            'position' => 'integer|required',
        ]);

        $faq->update([
            'question' => $request->get('question'),
            'answer' => '',
            'position' => $request->get('position'),
            'faq-trixFields' => request('faq-trixFields'),
        ]);

        return redirect()->route('admin.faq');
    }

    function delete(Faq $faq){
        $faq->delete();
        return redirect()->route('admin.faq');
    }
}
