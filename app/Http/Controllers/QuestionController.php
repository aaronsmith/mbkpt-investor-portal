<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginValidation;
use App\Http\Requests\PasswordValidation;
use App\Http\Requests\RequestValidation;
use App\Http\Requests\UserValidation;
use App\Mail\QuestionAnswered;
use App\Mail\UserRequest;
use App\Mail\ValidateEmail;
use App\Models\Admin;
use App\Models\Faq;
use App\Models\File;
use App\Models\Log;
use App\Models\Role;
use App\Models\User;
use App\Models\UserQuestion;
use Carbon\Carbon;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Mail;
use QrCode;
use Storage;
use setasign\Fpdi\Fpdi;
use Validator;


class QuestionController extends Controller
{

    function index(){
        $questions = UserQuestion::where('user_id', \Auth::user()->id)->orderBy('id')->get();

        if(\Auth::user()->isAbleTo('Make Questions') && $questions->isEmpty()) return view('questions.create');

        return view('questions.index', [
            'questions' => $questions
        ]);
    }

    function ask(){
        return view('questions.create');
    }

    function save(Request $request){
        Validator::extend('content_check', function($attribute, $value, $parameters){
            return !is_null($value['content']);
        });

        $messages = [
            'required' => 'You need to check the FAQ page first.',
            'content_check' => 'Question is required.',
        ];

        $rules = [
            'already_checked_faq' => 'required',
            'userquestion-trixFields' => 'content_check',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);


        $validator->validate();


        UserQuestion::create([
            'question' => '',
            'user_id' => \Auth::user()->id,
            'userquestion-trixFields' => request('userquestion-trixFields'),
        ]);

        return redirect()->route('questions');
    }

    function adminIndex(){
        $questions = UserQuestion::whereNull('answer_date')->orderBy('id')->get();
        return view('admin.questions.index', [
            'questions' => $questions
        ]);
    }

    function answer(UserQuestion $question){
        return view('admin.questions.answer', [
            'question' => $question
        ]);
    }

    function saveAnswer(Request $request, UserQuestion $question){
        Mail::to($question->user->email)->send(new QuestionAnswered($question->user, $question));

        $question->update([
            'answer_date' => Carbon::now(),
            'answered_by' => \Auth::guard('admin')->user()->id,
            'userquestion-trixFields' => request('userquestion-trixFields'),
        ]);

        return redirect()->route('admin.questions');
    }

    function view(UserQuestion $question){
        return view('questions.view', [
            'question' => $question
        ]);
    }

    function history(){
        $is_answered = '';
        $search_st = '';

        $questions = UserQuestion::orderBy('id');

        if(request()->has('is_answered') && !empty(request()->get('is_answered'))){
            $is_answered = request()->get('is_answered');

            if($is_answered == 'yes'){
                $questions = $questions->whereNotNull('answer_date');
            }else{
                $questions = $questions->whereNull('answer_date');
            }
        }


        if(request()->has('search_st') && !empty(request()->get('search_st'))){
            $search_st = request()->get('search_st');
            $questions = $questions->trixRichText->where('content', 'like', '%' . $search_st . '%');
        }

        $questions = $questions->paginate(20)->withQueryString();


        return view('admin.question-history', [
            'questions' => $questions,
            'is_answered' => $is_answered,
            'search_st' => $search_st
        ]);
    }

    function adminView(UserQuestion $question){
        return view('admin.questions.view', [
            'question' => $question
        ]);
    }
}
