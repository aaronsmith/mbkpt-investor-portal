<?php

namespace App\Mail;

use App\Models\User;
use App\Models\UserQuestion;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class QuestionAnswered extends Mailable
{
    use Queueable, SerializesModels;

    public $question;
    public $user;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, UserQuestion $question)
    {
        $this->user = $user;
        $this->question = $question;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(env('MAIL_FROM_ADDRESS'))
            ->subject("MBKPT Investor Portal - Question Answered")
            ->markdown('emails.answered', [
                'user' => $this->user,
                'question' => $this->question,
            ]);
    }
}
