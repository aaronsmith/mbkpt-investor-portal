<?php

namespace Database\Seeders;

use App\Models\LogItem;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        /*LogItem::create(['id' => 1, 'name' => 'Login successful']);
        LogItem::create(['id' => 3, 'name' => 'Incorrect login attempt']);
        LogItem::create(['id' => 4, 'name' => 'Validation email sent']);
        LogItem::create(['id' => 5, 'name' => 'Auto validation failed']);
        LogItem::create(['id' => 6, 'name' => 'Email address validated']);
        LogItem::create(['id' => 7, 'name' => 'Password created']);
        LogItem::create(['id' => 8, 'name' => 'Password updated']);
        LogItem::create(['id' => 9, 'name' => 'Logout']);
        LogItem::create(['id' => 10, 'name' => 'Manual validation requested']);
        LogItem::create(['id' => 11, 'name' => 'Request approved']);
        LogItem::create(['id' => 12, 'name' => 'File downloaded']);
        LogItem::create(['id' => 13, 'name' => 'User entered a valid code']);
        LogItem::create(['id' => 15, 'name' => 'Validation document generated for user']);


        $this->call([PermissionSeeder::class]);*/

        foreach(User::all() as $user){
            $user->syncRoles([1,2]);
        }
    }
}
