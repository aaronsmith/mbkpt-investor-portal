const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

const host = process.env.APP_URL.split('//')[1];
const port = '8181';

mix.options({
    hmrOptions: {
        host: host,
        port: port
    }
})

mix.js('resources/js/app.js', 'public/js')
    //.browserSync('trustee.local')
    .postCss('resources/css/app.css', 'public/css', [
        require("@tailwindcss/postcss7-compat"),
    ]);
